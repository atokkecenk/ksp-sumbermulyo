<?php
if (isset($_POST["login"])) {
    $pass = md5($_POST['pass']);

    $log = mysql_query("SELECT
        u.id_karyawan,
        u.nama,
        u.username,
        u.level
    FROM
        tb_karyawan u
    WHERE
        u.username = '$_POST[user]' 
        AND u.`password` = '$pass'");

    if (mysql_num_rows($log) > 0) {
        $data_user = mysql_fetch_array($log);
        session_start();

        $_SESSION['session_id']      = $data_user['id_karyawan'];
        $_SESSION['session_nama']      = $data_user['nama'];
        $_SESSION['session_level']     = $data_user['level'];
        $_SESSION['session_user']       = $data_user['username'];
        $_SESSION['session_log']       = 1;

        echo "<meta http-equiv='refresh'content='0;url=index.php?page=home'> ";
        exit;
    } else {
        echo "<script>alert('Username atau Password mungkin salah')</script>";
    }
}
?>
<!DOCTYPE html>
<html lang="en">

<meta http-equiv="content-type" content="text/html;charset=utf-8" /><!-- /Added by HTTrack -->

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Koperasi Simpan Pinjam - Sumber Mulyo</title>

    <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,400i,700&amp;display=fallback">
    <link rel="stylesheet" href="plugins/fontawesome-free/css/all.min.css">
    <link rel="stylesheet" href="plugins/icheck-bootstrap/icheck-bootstrap.min.css">
    <link rel="stylesheet" href="dist/css/adminlte.min.css">
</head>

<body class="hold-transition login-page">
    <div class="login-box">
        <div class="login-logo">
            <a href="index.php"><b>Sumber</b> Mulyo</a>
        </div>
        <div class="card">
            <div class="card-body login-card-body">
                <p class="login-box-msg">Sign in to start your session</p>

                <form action="" method="post">
                    <div class="input-group mb-3">
                        <input type="text" name="user" class="form-control" placeholder="Email">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-envelope"></span>
                            </div>
                        </div>
                    </div>
                    <div class="input-group mb-3">
                        <input type="password" name="pass" class="form-control" placeholder="Password">
                        <div class="input-group-append">
                            <div class="input-group-text">
                                <span class="fas fa-lock"></span>
                            </div>
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-8">
                            <div class="icheck-primary">
                                <input type="checkbox" id="remember">
                                <label for="remember">
                                    Remember Me
                                </label>
                            </div>
                        </div>
                        <div class="col-4">
                            <button type="submit" name="login" class="btn btn-primary btn-block">Sign In</button>
                        </div>
                    </div>
                </form>

            </div>
        </div>
    </div>
    <script src="plugins/jquery/jquery.min.js"></script>
    <script src="plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
    <script src="dist/js/adminlte.min.js"></script>
</body>

</html>