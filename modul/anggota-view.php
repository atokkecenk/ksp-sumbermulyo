<?php

?>
<div class="container-fluid">
    <div class="col-12">
        <div class="card card-info">
            <div class="card-header ui-sortable-handle">
                <h3 class="card-title">Anggota Koperasi</h3>

                <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Minimize data">
                        <i class="fas fa-minus"></i>
                    </button>
                    <a href="?page=anggota" class="btn btn-tool" title="Tambah Anggota">
                        <i class="fas fa-plus"></i>
                    </a>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" title="Tutup Data">
                        <i class="fas fa-times"></i>
                    </button>
                </div>
            </div>
            <div class="card-body">
                <table id="example2" class="table table-bordered table-hover">
                    <thead>
                        <tr>
                            <th class="text-center" width="5%">No.</th>
                            <th class="text-center">Nama</th>
                            <th class="text-center">No. KTP</th>
                            <th class="text-center">Jenis Kelamin</th>
                            <th class="text-center">Tanggal Masuk</th>
                            <th class="text-center">Status Anggota</th>
                            <th class="text-center"><i class="fas fa-cog"></i></th>
                        </tr>
                    </thead>
                    <tbody>
                        <?php
                        $no = 1;
                        foreach (get_anggota('result', '', '') as $val) {
                            $na = ($val['na'] == 'N') ? '<i class="fas fa-check-circle text-success" data-toggle="tooltip" data-placement="top" title="Aktif" style="cursor: help;"></i>' : '<i class="fas fa-ban text-danger" data-toggle="tooltip" data-placement="top" title="Nonaktif" style="cursor: help;"></i>';
                        ?>
                            <tr>
                                <td align="center"><?= $no ?></td>
                                <td><?= $val['nama'] ?></td>
                                <td><?= $val['ktp'] ?></td>
                                <td><?= $val['jk'] ?></td>
                                <td align="center"><?= $val['tgl_masuk'] ?></td>
                                <td><?= $na . ' - ' . $val['sts_anggota'] ?></td>
                                <td align="center">
                                    <a href="?aks=upd&page=anggota&ia=<?= $val['id'] ?>" class="btn btn-info btn-sm" title="Perbarui data"><i class="fas fa-edit"></i></a>
                                    <a href="?aks=upd&page=anggota&sub=mundur&id=<?= $val['id'] ?>" class="btn btn-danger btn-sm" title="Pengunduran diri"><i class="fas fa-times"></i></a>
                                    <!-- <img src="./dist/img/icon/edit.png" width="22" style="cursor: pointer;" onclick="location='?aks=upd&page=anggota&ia=<?= $val['id'] ?>'" title="Perbarui data"> -->
                                </td>
                            </tr>
                        <?php
                            $no++;
                        }
                        ?>
                    </tbody>
                </table>
            </div>
        </div>
    </div>
</div>

<script>
    $('#example2').DataTable({
        "paging": true,
        "lengthChange": false,
        "searching": true,
        "ordering": true,
        "info": true,
        "autoWidth": false,
        "responsive": true,
    });
</script>