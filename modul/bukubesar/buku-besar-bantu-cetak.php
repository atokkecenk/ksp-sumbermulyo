
<?php
require('../../libs/fpdf/fpdf.php');
require('../../config/db.php');

//require "../pages/plan/perintahprosesing.php";
class PDF extends FPDF
{



// Page header
    function Header()
    {
        // Logo
        $kd_rek         = $_GET['kd_rek'];

        $this->Image('../../images/logo.png', 10, 8, 50, 15);
        $this->SetFont('Arial', 'B', 14);

        $this->SetX(60);
        $this->Cell(150, 0, 'PT. BENIH CITRA ASIA', 0, 0, 'R');
        $this->SetFont('Arial', '', 8);
        //$this->Cell(35, 5, 'No. Dokumen', 'TRBL', 0, 'L');
        //$this->Cell(30, 5, 'CM-PLN-01-22', 'TRBL', 0, 'L');
        $this->Ln(5);
        $this->SetX(60);
        $this->SetFont('Arial', '', 10);
        $this->Cell(150, 0, 'Jl. Akmaludin No.26, PO. BOX 26 Jember 68175', '', '', 'R');
        $this->SetFont('Arial', '', 8);
        //$this->Cell(35, 5, 'Tanggal mulai berlaku', 'TRBL', 0, 'L');
        //$this->Cell(30, 5, '02-01-2017', 'TRBL', 0, 'L');
        $this->Ln(5);
        $this->SetX(60);
        $this->SetFont('Arial', '', 10);
        $this->Cell(150, 0, 'Jawa Timur - Indonesia', '', '', 'R');
        $this->SetFont('Arial', '', 8);
        //$this->Cell(35, 5, 'Rev : 0', 'TRBL', 0, 'L');
        //$this->Cell(30, 5, 'Hal: 1', 'TRBL', 0, 'L');
        $this->Ln(5);

        $this->SetFont('Arial','B',12);
        $this->SetFillColor(0, 179, 0);
        $this->SetDrawColor(0, 0, 0);
        $this->Cell(205,8,"BUKU BESAR",1,1,'C','C');
        $this->Ln(0);

        $this->SetFont('Arial','',10);
        //$this->Cell(205,5,'Kode Akun : '.$kdrek,0,0,'C');
        //$this->Cell(30,5,$tglmulai. ' s/d '.$tglakhir,0,0,'L');
        $this->Ln(5);

    }

// Page footer
    function Footer()
    {
        // Position at 1.5 cm from bottom
        $this->SetY(-15);
        //$this->Line(05,$this->GetY(),210,$this->getY());
        $this->SetFont('Arial','I',8);
        // Page number
        $this->Cell(0,15,'Halaman '.$this->PageNo().'/{nb}',0,0,'R');
        $this->setx(5);
        $this->Cell(5,15,'Print Out : '.date("d/m/Y H:i:s"),0,0,'L');
    }
};

    // kd_rek=21110&tglmulai=2019-01-01&tglakhir=2019-03-07&divisi=&data_jenis=SUPPLIER%20MATERIAL&supplier=SP-001&uraian=
$rekening       = $_GET['kd_rek'];
// $tglmulai       = $_GET['tglmulai'];
$tglmulai       = date('Y-m-d', strtotime('-1 days', strtotime($_GET['tglmulai'])));
$tglakhir       = $_GET['tglakhir'];
$divisi         = $_GET['divisi'];
$jenis          = $_GET['data_jenis'];
$supplier       = $_GET['supplier'];
$uraian         = $_GET['uraian'];
$nobukti        = $_GET['nobukti'];
$kodetransaksi  = $_GET['kodetransaksi'];

if ($divisi == ''){ $cari="";}
else {$cari="AND id_divisi='$divisi' ";}

if ($jenis == ''){ $carijenis="";}
else{
    $data_jenis = str_replace("_"," ",$jenis);
    $carijenis="AND a.jenis_identitas='$data_jenis' ";
}

if ($supplier == ''){ $carisupp="";}
else {$carisupp="AND id_identitas='$supplier' ";}

if ($uraian == ''){ $cariuraian="";}
else {$cariuraian="AND uraian like '%$uraian%' ";}

if ($nobukti == ''){ $carinobukti="";}
else {$carinobukti="AND no_bukti like '%$nobukti%' ";}

if ($kodetransaksi == ''){ $carikodetransaksi="";}
else {$carikodetransaksi="AND kode_transaksi like '%$kodetransaksi%' ";}

if ($rekening == ''){ $carirekening = ""; }
else {$carirekening = " and kd_rek = '$rekening'";}

$kategori=mysql_query("
                        SELECT
                                kd_rek as kd_rek,
                                idrek5 as idrek5,
                                `namarek5` as namarek5
                            FROM
                                v_kdrek t1
                            where kd_rek = '$rekening'
                    ") or die (mysql_error());


// Instanciation of inherited class
$pdf = new PDF("P", "mm", array(220, 330));
$pdf->setLeftMargin(7);
$pdf->AliasNbPages();
$pdf->AddPage();
$pdf->SetAutoPageBreak(true, 10);

$pdf->SetFont('Arial','BU',12);
// $pdf->Cell(210,10,'BUKU BESAR',10,'C','C');
//$pdf->Ln(1);

$k=mysql_fetch_array($kategori);
    $carikdrek=$k['kd_rek'];

    $pdf->SetFillColor(0,206,209);
//<tr><td colspan=6 align=center>$k[kode_rekening] $k[nama_rekening]</td></tr>
    $pdf->SetFont('Arial','B',10);

    $pdf->Cell(205,7,$k['kd_rek'].' - '.$k['namarek5'],'LRBT',0,'L','L');
    //$pdf->Cell(102.5,7,$k['namarek5'],'RT',0,'R','R');
    $pdf->Ln();

    //Warna Tabel

    $pdf->SetFillColor(220,220,220);
    //$pdf->SetDrawColor(128,10,0);
    $pdf->SetLineWidth(0);

    $pdf->SetFont('Arial','B',7);
    $pdf->Cell(8,7,'No.','LRT',0,'C','C');
    $pdf->Cell(20,7,'Tanggal','LRT',0,'C','C');
	$pdf->Cell(32,7,'Identitas','LRT',0,'C','C');
    $pdf->Cell(70,7,'Uraian','LRT',0,'C','C');
    $pdf->Cell(25,7,'Debet','LRT',0,'C','C');
    $pdf->Cell(25,7,'Kredit','LRT',0,'C','C');
    $pdf->Cell(25,7,'Saldo','LRT',0,'C','C');

    $pdf->Ln();

    $pdf->SetFont('Arial','',7);
    $saldoawal = "SELECT
                    `k`.`kd_rek`    AS `kd_rek`,
                    (SUM(IF((`k`.`idrek5` = `a`.`debet`),`a`.`jumlah`,0)) - SUM(IF((`k`.`idrek5` = `a`.`kredit`),`a`.`jumlah`,0))) AS `saldo`
                  FROM (`fnc_jurnal` `a`
                     JOIN `v_kdrek` `k`
                       ON (((`k`.`idrek5` = `a`.`debet`)
                             OR (`k`.`idrek5` = `a`.`kredit`))))
                  WHERE (`a`.`tanggal` BETWEEN '2015-01-01'
                         AND '$tglmulai') $cari $carirekening $carijenis $carisupp $cariuraian $carinobukti $carikodetransaksi
                  GROUP BY `k`.`idrek5`
                  ORDER BY `k`.`kd_rek`
                ";
            // echo $saldoawal;
    $querysa = mysql_query($saldoawal);
    $rowsa = mysql_fetch_array($querysa);          

    $pdf->Cell(8,5,'','BLTR',0,'C');
    $pdf->Cell(20,5,'','BTR',0,'C');
    $pdf->Cell(32,5,'','BTR',0,'C');
    $pdf->Cell(70,5,'Saldo Awal','BTR',0,'L');
    $pdf->Cell(25,5,'','BTR',0,'R');
    $pdf->Cell(25,5,'','BTR',0,'R');
    $pdf->Cell(25,5,number_format($rowsa['saldo'],0),'BTR',0,'R');

    $pdf->Ln();
    $jurnal=mysql_query("SELECT
                            a.`id_jurnal`,
                            a.`no_bukti`,
                            a.`kode_transaksi`,
                            a.`tanggal`,
                            a.`uraian`,
                            a.`jumlah` AS `debet`,
                            0 AS `kredit`,
                            b.nama_supplier
                          FROM
                            `fnc_jurnal` a
                            LEFT JOIN `view_identitas` b
                              ON a.`id_identitas` = b.`kode_supplier`
                          WHERE a.tanggal BETWEEN '$_GET[tglmulai]'
                            AND '$tglakhir'
                            AND a.`debet` = '$k[idrek5]' $cari $carijenis $carisupp $cariuraian $carinobukti $carikodetransaksi
                          UNION
                          ALL
                          SELECT
                            a.`id_jurnal`,
                            a.`no_bukti`,
                            a.`kode_transaksi`,
                            a.`tanggal`,
                            a.`uraian`,
                            0 AS `debet`,
                            a.`jumlah` AS `kredit`,
                            b.nama_supplier
                          FROM
                            `fnc_jurnal` a
                            LEFT JOIN `view_identitas` b
                              ON a.`id_identitas` = b.`kode_supplier`
                          WHERE a.tanggal BETWEEN '$_GET[tglmulai]'
                            AND '$tglakhir'
                            AND a.`kredit` = '$k[idrek5]' $cari $carijenis $carisupp $cariuraian $carinobukti $carikodetransaksi
                        ORDER BY `tanggal` ASC
                    ") or die (mysql_error());

    $no=1;
    $tsaldo=0;
    $tnilai=0;
    $tterbayar=0;
    $saldo=$rowsa['saldo'];  

    while ($data=mysql_fetch_array($jurnal)){

        if ($data['debet']>0 )
        {
            $tsaldo+=$data['debet'] ;
        }
        else
        {
            $tsaldo-=$data['debet']+$data['kredit'] ;
        }

        $saldo=($saldo+$data['debet'])- $data['kredit']; 

        $pdf->SetFont('Arial','',7);
        $pdf->Cell(8,5,$no,'BLTR',0,'C');
        $pdf->Cell(20,5,$data['tanggal'],'BTR',0,'C');
		$pdf->Cell(32,5,$data['nama_supplier'],'BTR',0,'C');
        $pdf->Cell(70,5,$data['uraian'],'BTR',0,'L');
        $pdf->Cell(25,5,number_format($data['debet'],0),'BTR',0,'R');
        $pdf->Cell(25,5,number_format($data['kredit'],0),'BTR',0,'R');
        $pdf->Cell(25,5,number_format($saldo,0),'BTR',0,'R');

        $pdf->Ln();

        $no++;

        $tnilai=$tnilai+$data['debet'];
        $tterbayar=$tterbayar+$data['kredit'];

    }


    $pdf->SetFillColor(220, 225, 0);
    $pdf->SetFont('Arial','B',7);

    $pdf->Cell(8,5,'','BLTR',0,'C');
    $pdf->Cell(20,5,'','BTR',0,'C');
    $pdf->Cell(32,5,'','BTR',0,'C');
    $pdf->Cell(70,5,'Total per Pencarian :','BTR',0,'L');
    $pdf->Cell(25,5,number_format($tnilai, 0),'BTR',0,'R');
    $pdf->Cell(25,5,number_format($tterbayar, 0),'BTR',0,'R');
    $pdf->Cell(25,5,number_format(($tnilai-$tterbayar) ,0),'BTR',0,'R');

    $pdf->Ln();

    $pdf->ln(5);
//}
$pdf->ln();


$pdf->Output('Buku Besar.pdf','I');

?>