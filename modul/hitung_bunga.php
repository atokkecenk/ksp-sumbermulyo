<?php
switch ($_GET['aks']) {
    case 'data':
        require('../db.php');
        $bln = $_POST["bl"];
        $thn = $_POST["th"];
        $hr = cal_days_in_month(CAL_GREGORIAN, $bln, $thn);
        $tgl_akhir = $thn . '-' . $bln . '-' . $hr;
        $q_jasa = mysql_query("SELECT
            r.nama_anggota,
            r.no_rekening,
            r.jasa_persen,
            m.saldo - m.jasa as saldo,
            m.jasa
        FROM
            tb_mutasi m
            JOIN tb_rekening r on r.id_rekening = m.id_rekening
        where m.tanggal = '$tgl_akhir' and m.jasa > 0");
        $no = 1;
        while ($r_jasa = mysql_fetch_array($q_jasa)) {
?>
            <tr>
                <td><?= $no++; ?></td>
                <td><?= $r_jasa['nama_anggota']; ?></td>
                <td><?= $r_jasa['no_rekening']; ?></td>
                <td><?= ($r_jasa['jasa_persen'] * 100) . "%"; ?></td>
                <td><?= number_format($r_jasa['saldo'], 0, ',', '.'); ?></td>
                <td><?= number_format($r_jasa['jasa'], 0, ',', '.'); ?></td>
            </tr>
        <?php
        }
        exit;
        break;

    default:
        $bln = $_POST["bulan"];
        $thn = $_POST["tahun"];
        $hr = cal_days_in_month(CAL_GREGORIAN, $bln, $thn);
        $tgl_awal = $thn . '-' . $bln . '-01';
        $tgl_akhir = $thn . '-' . $bln . '-' . $hr;
        ?>
        <div class="container-fluid">
            <div class="card card-default">
                <div class="card-header">
                    <h3 class="card-title">Hitung Jasa</h3>

                    <div class="card-tools">
                        <!-- <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button> -->
                        <!-- <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button> -->
                    </div>
                </div>
                <div class="card-body">
                    <form class="form-horizontal" method="post" name="cari" action="<?php echo $_SERVER['REQUEST_URI'] ?>">
                        <div class="card-body">
                            <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Tahun</label>
                                        <select name="tahun" id="tahun" class="form-control">
                                            <option value="">--Pilih Tahun--</option>
                                            <?php
                                            for ($i = date('Y'); $i > 2020; $i--) {
                                                echo "<option value='$i'>$i</option>";
                                            }
                                            ?>
                                        </select>
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label>Bulan</label>
                                        <select name="bulan" id="bulan" class="form-control">
                                            <option value="">--Pilih Bulan--</option>
                                            <option value="01">Januari</option>
                                            <option value="02">Februari</option>
                                            <option value="03">Maret</option>
                                            <option value="04">April</option>
                                            <option value="05">Mei</option>
                                            <option value="06">Juni</option>
                                            <option value="07">Juli</option>
                                            <option value="08">Agustus</option>
                                            <option value="09">September</option>
                                            <option value="10">Oktober</option>
                                            <option value="11">Nopember</option>
                                            <option value="12">Desember</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="box-footer">
                            <div class="text-center">
                                <button type="submit" name="hitung" class="btn btn-md btn-primary btn-flat"><i class="fa fa-search"></i> Hitung</button>
                            </div>
                        </div>
                    </form>
                </div>
                <div class="table-responsive">
                    <table id="example2" class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th class="text-center" width="5%">No.</th>
                                <th class="text-left">Nama</th>
                                <th class="text-center">No. Rekening</th>
                                <th class="text-right">Jasa</th>
                                <th class="text-right">Saldo</th>
                                <th class="text-right">Nilai Jasa</th>
                            </tr>
                        </thead>
                        <tbody id="tbl-jasa">
                            <?php
                            if (isset($_POST["hitung"])) {
                                $no = 1;
                                $q_simpanan = mysql_query("SELECT * from tb_produksimpanan");
                                while ($r_simpanan = mysql_fetch_array($q_simpanan)) {
                                    $q_rekening = mysql_query("SELECT * FROM tb_rekening where jproduk = '$r_simpanan[kode_produk]'");
                                    while ($r_rekening = mysql_fetch_array($q_rekening)) {
                                        $get_kd = mysql_fetch_array(mysql_query("SELECT kode from tb_anggota where id = '$r_rekening[id_anggota]'"));
                                        //! perulangan hitung saldo
                                        $persen = $r_rekening['jasa_persen'];
                                        $saldo = mysql_fetch_array(mysql_query("SELECT * FROM tb_mutasi where id_rekening = '$r_rekening[id_rekening]' and tanggal < '$tgl_awal' order by tanggal desc limit 1"));
                                        $saldonya = $saldo['saldo'];
                                        $jasa = 0;
                                        $sum = 0;
                                        $pembagi = 0;
                                        for ($i = 1; $i < $hr; $i++) {
                                            $jasa += round($saldonya * $persen);
                                            $bayar = 0;
                                            $tarik = 0;
                                            $tglnya = $thn . '-' . $bln . '-' . $i;
                                            $cek_km = mysql_fetch_array(mysql_query("SELECT * FROM tb_mutasi where id_rekening = '$r_rekening[id_rekening]' and tanggal = '$tglnya'"));
                                            $bayar = $cek_km['bayar'];
                                            $tarik = $cek_km['tarik'];
                                            if ($saldonya > 0) {
                                                $sum += $saldonya;
                                                $pembagi += 1;
                                            }
                                            $saldonya = $saldonya + $bayar - $tarik;

                                            // echo $tglnya . " | " . $saldonya . " | " . $jasa . "|" . $sum . "|" . $pembagi . "<br>";
                                        }

                                        if ($r_rekening['id_jasa_rumus'] == '1') {
                                            $jasa_masuk = $jasa;
                                        } else {
                                            $jasa_masuk = round((($sum / $pembagi) * $persen) * $pembagi);
                                        }

                                        $result = mysql_fetch_array(mysql_query("SELECT r.id_rekening, v.saldonya, v.limit_simpanan, 
                                            if(v.NS is null,'NO',v.NS) as NS
                                            FROM tb_rekening r
                                            left join view_saldosimpanan v on r.id_rekening = v.id_rekening
                                            WHERE r.id_rekening='$r_rekening[id_rekening]'"));

                                        $cek_mutasi = mysql_query("SELECT * FROM tb_mutasi where id_rekening = '$r_rekening[id_rekening]' and tanggal = '$tgl_akhir' and jasa > 0");
                                        if (mysql_num_rows($cek_mutasi) == 0) {
                            ?>
                                            <tr>
                                                <td><?= $no++ ?></td>
                                                <td><?= $r_rekening['nama_anggota']; ?></td>
                                                <td><?= $r_rekening['no_rekening']; ?></td>
                                                <td class="text-right"><?= ($r_rekening['jasa_persen'] * 100) . "%"; ?></td>
                                                <td class="text-right"><?= number_format($result['saldonya'], 0, ',', '.'); ?></td>
                                                <td class="text-right"><?= number_format($jasa, 0, ',', '.'); ?></td>
                                            </tr>

                            <?php

                                            $saldo_akhir = $result["saldonya"] + $jasa_masuk;
                                            //todo insert ke tb_mutasi
                                            $ins = mysql_query("INSERT INTO tb_mutasi (id_rekening, jtransaksi, no_mutasi, tanggal,jasa, nilai, saldo, keterangan, tgl_save)
                                            VALUES ('$r_rekening[id_rekening]','$r_rekening[jproduk]','$_POST[no_mutasi]','$tgl_akhir','$jasa_masuk','$jasa_masuk','$saldo_akhir','Jasa akhir bulan',CURDATE())");

                                            //todo insert debet ke jurnal
                                            $ins = mysql_query("INSERT INTO tb_jurnal (no_bukti, kode_transaksi, tanggal, debet, kredit, uraian, jumlah, id_identitas, tgl_input, user_input, jenis_identitas, idrek1, kdrek1, idrek2, kdrek2, idrek3, kdrek3, idrek4, kdrek4, namarek1, namarek2, namarek3, namarek4)
                                            SELECT '$_POST[no_mutasi]','$get_kd[kode]','$tgl_akhir',idrek4,0,'Jasa akhir bulan','$jasa_masuk','$get_kd[kode]',CURDATE(),'$_SESSION[session_user]','ANGGOTA', idrek1, kdrek1, idrek2, kdrek2, idrek3, kdrek3, idrek4, kdrek4, namarek1, namarek2, namarek3, namarek4 FROM v_rekening where idrek4 = '$r_rekening[idrek_debet_jasa]'");

                                            //todo insert kredit ke jurnal
                                            $ins = mysql_query("INSERT INTO tb_jurnal (no_bukti, kode_transaksi, tanggal, debet, kredit, uraian, jumlah, id_identitas, tgl_input, user_input, jenis_identitas, idrek1, kdrek1, idrek2, kdrek2, idrek3, kdrek3, idrek4, kdrek4, namarek1, namarek2, namarek3, namarek4)
                                            SELECT '$_POST[no_mutasi]','$get_kd[kode]','$tgl_akhir',0,idrek4,'Jasa akhir bulan','$jasa_masuk','$get_kd[kode]',CURDATE(),'$_SESSION[session_user]','ANGGOTA', idrek1, kdrek1, idrek2, kdrek2, idrek3, kdrek3, idrek4, kdrek4, namarek1, namarek2, namarek3, namarek4 FROM v_rekening where idrek4 = '$r_rekening[idrek_kredit_jasa]'");
                                        }
                                    }
                                }
                            }
                            ?>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
        <script>
            $('#bulan').on('change', function() {
                var tahun = $('#tahun').val();
                var bulan = $('#bulan').val();
                $.ajax({
                    type: "post",
                    url: "modul/hitung_bunga.php?&aks=data",
                    data: {
                        th: tahun,
                        bl: bulan
                    },
                    success: function(response) {
                        $('#tbl-jasa').html(response);
                    }
                });
            });
        </script>
<?php
        break;
}
?>