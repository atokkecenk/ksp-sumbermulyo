<?php
if (isset($_POST['filter'])) {
    if (($_POST['tgl1'] == "") && ($_POST['tgl2']) == "") {
        $tgl1 = "2008-01-01";
        $tgl2 = date('Y-m-d', strtotime('+30 days', strtotime(date('Y-m-d'))));
    } else {
        $tgl1 = $_POST['tgl1'];
        $tgl2 = $_POST['tgl2'];
        $tglawal = date('Y-m-d', strtotime('-1 days', strtotime($tgl1)));
    }
    $jenis          = $_POST['jenis_identitas'];
    $identitas      = $_POST['id_identitas'];
    $uraian         = $_POST['uraian'];
    $nobukti        = $_POST['nobukti'];
    $kodetransaksi  = $_POST['kodetransaksi'];
    $rekening       = $_POST['rekening'];

    if ($jenis == '') {
        $carijenis = "";
    } else {
        $data_jenis = str_replace("_", " ", $jenis);
        $carijenis = "AND a.jenis_identitas='$data_jenis' ";
    }

    if ($identitas == '') {
        $cariidentitas = "";
    } else {
        $cariidentitas = "AND id_identitas='$identitas' ";
    }

    if ($uraian == '') {
        $cariuraian = "";
    } else {
        $cariuraian = "AND uraian like '%$uraian%' ";
    }

    if ($nobukti == '') {
        $carinobukti = "";
    } else {
        $carinobukti = "AND no_bukti like '%$nobukti%' ";
    }

    if ($kodetransaksi == '') {
        $carikodetransaksi = "";
    } else {
        $carikodetransaksi = "AND kode_transaksi like '%$kodetransaksi%' ";
    }

    if ($rekening == '') {
        $carirekening = "";
    } else {
        $carirekening = " AND idrek4='$rekening' ";
    }
}
?>
<div class="container-fluid">
    <div class="card card-default">
        <div class="card-header">
            <h3 class="card-title">Daftar Laba Rugi Unit Usaha</h3>

            <div class="card-tools">
                <!-- <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button> -->
                <!-- <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button> -->
            </div>
        </div>
        <div class="card-body">
            <form class="form-horizontal" action="" method="post">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tanggal Awal</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="far fa-calendar-alt"></i>
                                        </span>
                                    </div>
                                    <!-- <input type="date" name="tgl1" id="tgl1" class="form-control"> -->
                                    <input type="text" class="form-control form-control-sm datepicker-input tanggal" name="tgl1" id="tgl1" data-toggle="datetimepicker" data-target="#datetimepicker" placeholder="YYYY-MM-DD" autocomplete="off" value="<?php echo $_POST['tgl1'] ?>">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tanggal Akhir</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="far fa-calendar-alt"></i>
                                        </span>
                                    </div>
                                    <!-- <input type="date" name="tgl2" id="tgl2" class="form-control"> -->
                                    <input type="text" class="form-control form-control-sm datepicker-input tanggal" name="tgl2" id="tgl2" data-toggle="datetimepicker" data-target="#datetimepicker" placeholder="YYYY-MM-DD" autocomplete="off" value="<?php echo $_POST['tgl2'] ?>">

                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="box-footer">
                    <div class="text-center">
                        <button type="submit" name="filter" class="btn btn-sm btn-primary btn-flat"><i class="fa fa-search"></i> TAMPILKAN</button>
                        <button type="button" class="btn btn-sm btn-success btn-flat export"><i class="fa fa-file-excel"></i> EXCEL</button>
                    </div>
                </div>
        </div>
        <style>
            .kecilkan {
                padding-top: 0px;
                padding-bottom: 0px;
            }
        </style>
        <?php if ($_POST) { ?>
            <div class="table-responsive">
                <table class="table table-bordered table-hovered table-sm text-sm p-0 m-0" id="table">
                    <thead>
                        <tr class="bg-blue">
                            <th width="5%" class="text-center kecilkan">NO</th>
                            <th width="20%">KODE REKENING</th>
                            <th>NAMA REKENING</th>
                            <th width="10%">JUMLAH</th>
                        </tr>
                    </thead>
                    <th class="bg-warning" colspan="4" style="padding-top: 3px;padding-bottom: 3px;"><?= "<div style='text-align:left;'>PENDAPATAN<div>" ?></th>
                    <?php
                    $rekening4 = "SELECT
                                            `j`.`id_divisi` AS `id_divisi`,
                                            `k`.`idrek4`    AS `idrek4`,
                                            `k`.`kd_rek`    AS `kd_rek`,
                                            `k`.`namarek4`  AS `namarek4`,
                                            SUM(IF((`k`.`idrek4` = `j`.`debet`),`j`.`jumlah`,0)) AS `debet`,
                                            SUM(IF((`k`.`idrek4` = `j`.`kredit`),`j`.`jumlah`,0)) AS `kredit`,
                                            (SUM(IF((`k`.`idrek4` = `j`.`debet`),`j`.`jumlah`,0)) - SUM(IF((`k`.`idrek4` = `j`.`kredit`),`j`.`jumlah`,0))) AS `saldo`
                                            FROM (`tb_jurnal` `j`
                                            JOIN `v_rekening` `k`
                                                    ON (((`k`.`idrek4` = `j`.`debet`)
                                                            OR (`k`.`idrek4` = `j`.`kredit`))))
                                            WHERE (`j`.`tanggal` BETWEEN '$tgl1' AND '$tgl2') AND j.kdrek1='4'
                                            GROUP BY `k`.`idrek4`
                                            ORDER BY `k`.`kd_rek`
                                            ";
                    $no = 0;
                    $totalsaldo4 = 0;
                    $qrekening4 = mysql_query($rekening4);
                    while ($key4 = mysql_fetch_object($qrekening4)) {
                        $no++;
                        if ($key4->saldo < 0) {
                            $saldopendapatan = $key4->saldo * -1;
                        } else {
                            $saldopendapatan = $key4->saldo * -1;
                        }
                        $totalsaldo4 = $totalsaldo4 + $saldopendapatan;
                    ?>
                        <tr>
                            <td style="padding-top: 3px;padding-bottom: 3px;"><?= "<div style='text-align:center;'>" . $no . "<div>"; ?></td>
                            <td style="padding-top: 3px;padding-bottom: 3px;"><?= $key4->kd_rek; ?></td>
                            <td style="padding-top: 3px;padding-bottom: 3px;"><?= $key4->namarek4; ?></td>
                            <td style="padding-top: 3px;padding-bottom: 3px;"><?= "<div style='text-align:right;'>" . number_format($saldopendapatan) . "<div>"; ?></td>
                        </tr>
                    <?php } ?>
                    <tr class="bg-secondary">
                        <th style="padding-top: 3px;padding-bottom: 3px;" colspan="3">
                            <div style='text-align:center;'>TOTAL<div>
                        </th>
                        <th style="padding-top: 3px;padding-bottom: 3px;"><?= "<div style='text-align:right;'>" . number_format($totalsaldo4) . "<div>" ?></th>
                    </tr>
                    <th class="bg-warning" colspan="4" style="padding-top: 3px;padding-bottom: 3px;"><?= "<div style='text-align:left;'>BELANJA<div>" ?></th>
                    <?php
                    $rekening5 = "SELECT
                                            `j`.`id_divisi` AS `id_divisi`,
                                            `k`.`idrek4`    AS `idrek4`,
                                            `k`.`kd_rek`    AS `kd_rek`,
                                            `k`.`namarek4`  AS `namarek4`,
                                            SUM(IF((`k`.`idrek4` = `j`.`debet`),`j`.`jumlah`,0)) AS `debet`,
                                            SUM(IF((`k`.`idrek4` = `j`.`kredit`),`j`.`jumlah`,0)) AS `kredit`,
                                            (SUM(IF((`k`.`idrek4` = `j`.`debet`),`j`.`jumlah`,0)) - SUM(IF((`k`.`idrek4` = `j`.`kredit`),`j`.`jumlah`,0))) AS `saldo`
                                            FROM (`tb_jurnal` `j`
                                            JOIN `v_rekening` `k`
                                                    ON (((`k`.`idrek4` = `j`.`debet`)
                                                            OR (`k`.`idrek4` = `j`.`kredit`))))
                                            WHERE (`j`.`tanggal` BETWEEN '$tgl1' AND '$tgl2') AND j.kdrek1='5'
                                            GROUP BY `k`.`idrek4`
                                            ORDER BY `k`.`kd_rek`
                                            ";
                    $no = 0;
                    $totalsaldo5 = 0;
                    $qrekening5 = mysql_query($rekening5);
                    while ($key5 = mysql_fetch_object($qrekening5)) {
                        $no++;
                        $totalsaldo5 = $totalsaldo5 + $key5->saldo;
                    ?>
                        <tr>
                            <td style="padding-top: 3px;padding-bottom: 3px;"><?= "<div style='text-align:center;'>" . $no . "<div>"; ?></td>
                            <td style="padding-top: 3px;padding-bottom: 3px;"><?= $key5->kd_rek; ?></td>
                            <td style="padding-top: 3px;padding-bottom: 3px;"><?= $key5->namarek4; ?></td>
                            <td style="padding-top: 3px;padding-bottom: 3px;"><?= "<div style='text-align:right;'>" . number_format($key5->saldo) . "<div>"; ?></td>
                        </tr>
                    <?php } ?>
                    <tr class="bg-secondary">
                        <th style="padding-top: 3px;padding-bottom: 3px;" colspan="3">
                            <div style='text-align:center;'>TOTAL<div>
                        </th>
                        <th style="padding-top: 3px;padding-bottom: 3px;"><?= "<div style='text-align:right;'>" . number_format($totalsaldo5) . "<div>" ?></th>
                    </tr>
                    <tr class="bg-secondary">
                        <th style="padding-top: 3px;padding-bottom: 3px;" colspan="3">
                            <div style='text-align:center;'>LABA/RUGI<div>
                        </th>
                        <th style="padding-top: 3px;padding-bottom: 3px;"><?= "<div style='text-align:right;'>" . number_format($totalsaldo4 - $totalsaldo5) . "<div>" ?></th>
                    </tr>
                </table>
            </div>
        <?php } ?>
        </form>
    </div>
</div>