<?php
if (isset($_POST['filter'])) {
    if (($_POST['tgl1'] == "") && ($_POST['tgl2']) == "") {
        $tgl1 = "2008-01-01";
        $tgl2 = date('Y-m-d', strtotime('+30 days', strtotime(date('Y-m-d'))));
    } else {
        $tgl1 = $_POST['tgl1'];
        $tgl2 = $_POST['tgl2'];
        $tglawal = date('Y-m-d', strtotime('-1 days', strtotime($tgl1)));
    }
    $jenis          = $_POST['jenis_identitas'];
    $identitas      = $_POST['id_identitas'];
    $uraian         = $_POST['uraian'];
    $nobukti        = $_POST['nobukti'];
    $kodetransaksi  = $_POST['kodetransaksi'];
    $rekening       = $_POST['rekening'];

    if ($jenis == '') {
        $carijenis = "";
    } else {
        $data_jenis = str_replace("_", " ", $jenis);
        $carijenis = "AND a.jenis_identitas='$data_jenis' ";
    }

    if ($identitas == '') {
        $cariidentitas = "";
    } else {
        $cariidentitas = "AND id_identitas='$identitas' ";
    }

    if ($uraian == '') {
        $cariuraian = "";
    } else {
        $cariuraian = "AND uraian like '%$uraian%' ";
    }

    if ($nobukti == '') {
        $carinobukti = "";
    } else {
        $carinobukti = "AND no_bukti like '%$nobukti%' ";
    }

    if ($kodetransaksi == '') {
        $carikodetransaksi = "";
    } else {
        $carikodetransaksi = "AND kode_transaksi like '%$kodetransaksi%' ";
    }

    if ($rekening == '') {
        $carirekening = "";
    } else {
        $carirekening = " AND idrek4='$rekening' ";
    }
}
?>
<div class="container-fluid">
    <div class="card card-default">
        <div class="card-header">
            <h3 class="card-title">Neraca Unit Usaha</h3>

            <div class="card-tools">
                <!-- <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button> -->
                <!-- <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button> -->
            </div>
        </div>
        <div class="card-body">
            <form class="form-horizontal" action="" method="post">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="form-group">
                                <label>Sampai dengan Tanggal</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="far fa-calendar-alt"></i>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control form-control-sm datepicker-input tanggal" name="tgl1" id="tgl1" value="<?= $_POST['tgl1'] ?>" data-toggle="datetimepicker" data-target="#datetimepicker" placeholder="YYYY-MM-DD" autocomplete="off">
                                    <button type="submit" name="filter" class="btn btn-sm btn-primary btn-flat"><i class="fa fa-search"></i> TAMPILKAN</button>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="col-md-6">
                            <div class="form-group">
                                <label>Tanggal Awal</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="far fa-calendar-alt"></i>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control form-control-sm datepicker-input tanggal" name="tgl1" id="tgl1" data-toggle="datetimepicker" data-target="#datetimepicker" placeholder="YYYY-MM-DD" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tanggal Akhir</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="far fa-calendar-alt"></i>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control form-control-sm datepicker-input tanggal" name="tgl2" id="tgl2" data-toggle="datetimepicker" data-target="#datetimepicker" placeholder="YYYY-MM-DD" autocomplete="off">

                                </div>
                            </div> 
                    </div>-->
                    </div>
                </div>
                <div class="box-footer">
                    <div class="text-center">
                        <!-- <button type="submit" name="filter" class="btn btn-sm btn-primary btn-flat"><i class="fa fa-search"></i> TAMPILKAN</button> -->
                    </div>
                </div>
        </div>
        <style>
            .kecilkan {
                padding-top: 0px;
                padding-bottom: 0px;
            }
        </style>
        <?php if (!empty($_POST['tgl1'])) { ?>
            <div class="row">
                <div class="col-md-6">
                    <div class="table-responsive">
                        <table class="table table-hovered table-sm" id="table">
                            <thead>
                                <tr class="bg-blue">
                                    <th width="5%" class="text-center">NO</th>
                                    <th width="20%">KODE</th>
                                    <th>NAMA REKENING</th>
                                    <th width="10%">JUMLAH</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $total = 0;
                                $q1 = mysql_query("SELECT *, SUBSTRING(kd_rek, 1, 3) as kdrekxx FROM v_rekening WHERE kd_rek LIKE '1%' GROUP BY SUBSTRING(kd_rek, 1, 2)");
                                while ($r1 = mysql_fetch_array($q1)) {
                                ?>
                                    <tr class="bg-yellow">
                                        <th colspan="4"><?php echo $r1['kdrek1'] . ' ' . $r1['namarek1']; ?></th>
                                    </tr>
                                    <?php
                                    $n = 0;
                                    $total_saldo11 = 0;
                                    $tglawal = date('Y-m-d', strtotime('-1 days', strtotime($tgl1)));
                                    $rekening11 = "SELECT
                                            `j`.`id_divisi` AS `id_divisi`,
                                            `k`.`idrek4`    AS `idrek4`,
                                            `k`.`kd_rek`    AS `kd_rek`,
                                            `k`.`namarek4`  AS `namarek4`,
                                            SUM(IF((`k`.`idrek4` = `j`.`debet`),`j`.`jumlah`,0)) AS `debet`,
                                            SUM(IF((`k`.`idrek4` = `j`.`kredit`),`j`.`jumlah`,0)) AS `kredit`,
                                            (SUM(IF((`k`.`idrek4` = `j`.`debet`),`j`.`jumlah`,0)) - SUM(IF((`k`.`idrek4` = `j`.`kredit`),`j`.`jumlah`,0))) AS `saldo`
                                            FROM (`tb_jurnal` `j`
                                            JOIN `v_rekening` `k`
                                                    ON (((`k`.`idrek4` = `j`.`debet`)
                                                            OR (`k`.`idrek4` = `j`.`kredit`))))
                                            WHERE (`j`.`tanggal` BETWEEN '2008-01-01' AND '$tgl1') AND j.kdrek1='$r1[kdrek1]' 
                                            GROUP BY `j`.`kdrek1`,`j`.`kdrek2`
                                            ORDER BY `k`.`kd_rek`
                                            ";
                                    // echo $rekening11;
                                    $no = 0;
                                    $totalrekening11 = 0;
                                    $qrekening11 = mysql_query($rekening11);
                                    while ($key11 = mysql_fetch_object($qrekening11)) {
                                        $n++;
                                    ?>
                                        <tr>
                                            <td><?= "<div style='text-align:center;'>" . $n . "<div>"; ?></td>
                                            <td><?= $key11->kd_rek; ?></td>
                                            <td><?= $key11->namarek4; ?></td>
                                            <td>
                                                <?php
                                                echo "<div style='text-align:right;'>" . number_format($key11->saldo) . "<div>";
                                                ?>
                                            </td>
                                        </tr>
                                    <?php
                                        $total_saldo11 = $total_saldo11 + $key11->saldo;
                                    }
                                    $total = $total + $total_saldo11;
                                    ?>
                                    <tr class="bg-gray">
                                        <th class="text-right" colspan="3">JUMLAH:</th>
                                        <th width="10%" class="text-right"><?= number_format($total_saldo11); ?></th>
                                    </tr>
                                <?php } ?>

                                <tr class="bg-gray">
                                    <th class="text-right" colspan="3">TOTAL:</th>
                                    <th width="10%" class="text-right"><?= number_format($total); ?></th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
                <div class="col-md-6">
                    <div class="table-responsive">
                        <table class="table table-hovered table-sm p-0" id="table">
                            <thead>
                                <tr class="bg-blue">
                                    <th width="5%" class="text-center">NO</th>
                                    <th width="20%">KODE</th>
                                    <th>NAMA REKENING</th>
                                    <th width="10%">JUMLAH</th>
                                </tr>
                            </thead>
                            <tbody style="padding: 0px;">
                                <?php
                                $tglawalx = date_format(date_create($tglawal), "Y") . "-01-01";
                                $tglakhirx = $tglawal;
                                $getsaldo3611 = mysql_fetch_array(mysql_query("SELECT SUM(if(kdrek1='5',saldo*-1, saldo)) as saldo FROM (
                                                    SELECT
                                                    `j`.`id_divisi` AS `id_divisi`,
                                                    `k`.`kdrek1`    AS `kdrek1`,
                                                    `k`.`idrek4`    AS `idrek4`,
                                                    `k`.`kd_rek`    AS `kd_rek`,
                                                    `k`.`namarek4`  AS `namarek4`,
                                                    SUM(IF((`k`.`idrek4` = `j`.`debet`),`j`.`jumlah`,0)) AS `debet`,
                                                    SUM(IF((`k`.`idrek4` = `j`.`kredit`),`j`.`jumlah`,0)) AS `kredit`,
                                                    if((SUM(IF((`k`.`idrek4` = `j`.`debet`),`j`.`jumlah`,0)) - SUM(IF((`k`.`idrek4` = `j`.`kredit`),`j`.`jumlah`,0))) < 0,
                                                    (SUM(IF((`k`.`idrek4` = `j`.`debet`),`j`.`jumlah`,0)) - SUM(IF((`k`.`idrek4` = `j`.`kredit`),`j`.`jumlah`,0)))*-1,
                                                    (SUM(IF((`k`.`idrek4` = `j`.`debet`),`j`.`jumlah`,0)) - SUM(IF((`k`.`idrek4` = `j`.`kredit`),`j`.`jumlah`,0)))) AS `saldo`
                                                    FROM (`tb_jurnal` `j`
                                                    JOIN `v_rekening` `k`
                                                                    ON (((`k`.`idrek4` = `j`.`debet`)
                                                                                    OR (`k`.`idrek4` = `j`.`kredit`))))
                                                    WHERE (`j`.`tanggal` BETWEEN '$tglawalx' AND '$tgl1') AND (j.kdrek1='4' OR j.kdrek1='5')
                                                    GROUP BY `j`.`kdrek1`
                                                    ORDER BY `k`.`kd_rek`) a"));
                                $total = 0;
                                $q1 = mysql_query("SELECT *, SUBSTRING(kd_rek, 1, 3) as kdrekxx FROM v_rekening WHERE kd_rek LIKE '2%' OR kd_rek LIKE '3%' GROUP BY SUBSTRING(kd_rek, 1, 2)");
                                while ($r1 = mysql_fetch_array($q1)) {
                                ?>
                                    <tr class="bg-yellow">
                                        <th colspan="4"><?php echo $r1['kdrek1'] . ' ' . $r1['namarek2']; ?></th>
                                    </tr>
                                    <?php
                                    $n = 0;
                                    $total_saldo11 = 0;
                                    $tglawal = date('Y-m-d', strtotime('-1 days', strtotime($tgl1)));
                                    $rekening11 = "SELECT
                                            `j`.`id_divisi` AS `id_divisi`,
                                            `k`.`idrek4`    AS `idrek4`,
                                            `k`.`kd_rek`    AS `kd_rek`,
                                            `k`.`namarek4`  AS `namarek4`,
                                            SUM(IF((`k`.`idrek4` = `j`.`debet`),`j`.`jumlah`,0)) AS `debet`,
                                            SUM(IF((`k`.`idrek4` = `j`.`kredit`),`j`.`jumlah`,0)) AS `kredit`,
                                            (SUM(IF((`k`.`idrek4` = `j`.`debet`),`j`.`jumlah`,0)) - SUM(IF((`k`.`idrek4` = `j`.`kredit`),`j`.`jumlah`,0))) AS `saldo`
                                            FROM (`tb_jurnal` `j`
                                            JOIN `v_rekening` `k`
                                                    ON (((`k`.`idrek4` = `j`.`debet`)
                                                            OR (`k`.`idrek4` = `j`.`kredit`))))
                                            WHERE (`j`.`tanggal` BETWEEN '2008-01-01' AND '$tgl1') AND j.kdrek1='$r1[kdrek1]'
                                            GROUP BY `j`.`kdrek1`,`j`.`kdrek2`
                                            ORDER BY `k`.`kd_rek`
                                            ";
                                    $no = 0;
                                    $totalrekening11 = 0;
                                    $cek3611x = 0;
                                    $qrekening11 = mysql_query($rekening11);
                                    while ($key11 = mysql_fetch_object($qrekening11)) {
                                        $n++;
                                    ?>
                                        <tr>
                                            <td><?= "<div style='text-align:center;'>" . $n . "<div>"; ?></td>
                                            <td><?= $key11->kd_rek; ?></td>
                                            <td><?= $key11->namarek4; ?></td>
                                            <td>
                                                <?php
                                                if ($key11->saldo < 0) {
                                                    $saldorek2 = $key11->saldo * -1;
                                                } else {
                                                    $saldorek2 = $key11->saldo * -1;
                                                }
                                                // echo $key11->idrek4;
                                                $saldo3x = ($key11->idrek4 == 30) ? $getsaldo3611['saldo'] : $saldorek2;
                                                echo "<div style='text-align:right;'>" . number_format($saldo3x) . "<div>";
                                                ?>
                                            </td>
                                        </tr>
                                    <?php
                                        $total_saldo11 = $total_saldo11 + $saldo3x;

                                        $count3611 = ($key11->idrek4 == 30) ? $cek3611x + 1 : 0;
                                    }
                                    // echo $count3611;
                                    $nx = $n + 1;
                                    ?>
                                    <!-- cek rekening 3611 -->
                                    <?php if ($count3611 == 0 && $r1['kdrek1'] == 3) { ?>
                                        <tr>
                                            <td><?= "<div style='text-align:center;'>" . $nx . "<div>"; ?></td>
                                            <td>3 6 1 1</td>
                                            <td>SHU Tahun Berjalan</td>
                                            <td><?php
                                                echo "<div style='text-align:right;'>" . number_format($getsaldo3611['saldo']) . "<div>";
                                                ?>
                                            </td>
                                        </tr>
                                    <?php }



                                    if ($count3611 == 0 && $r1['kdrek1'] == 3) {
                                        $total_saldo11x = $total_saldo11 + $getsaldo3611['saldo'];
                                        $total = $total + $total_saldo11 + $getsaldo3611['saldo'];
                                    } else {
                                        $total_saldo11x = $total_saldo11;
                                        $total = $total + $total_saldo11;
                                    }
                                    ?>
                                    <tr class="bg-gray">
                                        <th class="text-right" colspan="3">JUMLAH:</th>
                                        <th width="10%" class="text-right"><?= number_format($total_saldo11x); ?></th>
                                    </tr>
                                <?php } ?>

                                <tr class="bg-gray">
                                    <th class="text-right" colspan="3">TOTAL:</th>
                                    <th width="10%" class="text-right"><?= number_format($total); ?></th>
                                </tr>

                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        <?php } ?>
        </form>
    </div>
</div>