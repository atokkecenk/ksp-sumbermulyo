<?php
if (isset($_POST['filter'])) {
    $tahun          = $_POST['tahun'];

    if ($jenis == '') {
        $carijenis = "";
    } else {
        $data_jenis = str_replace("_", " ", $jenis);
        $carijenis = "AND a.jenis_identitas='$data_jenis' ";
    }

    if ($identitas == '') {
        $cariidentitas = "";
    } else {
        $cariidentitas = "AND id_identitas='$identitas' ";
    }

    if ($uraian == '') {
        $cariuraian = "";
    } else {
        $cariuraian = "AND uraian like '%$uraian%' ";
    }

    if ($nobukti == '') {
        $carinobukti = "";
    } else {
        $carinobukti = "AND no_bukti like '%$nobukti%' ";
    }

    if ($kodetransaksi == '') {
        $carikodetransaksi = "";
    } else {
        $carikodetransaksi = "AND kode_transaksi like '%$kodetransaksi%' ";
    }

    if ($rekening == '') {
        $carirekening = "";
    } else {
        $carirekening = " AND idrek4='$rekening' ";
    }
}
?>
<div class="container-fluid">
    <div class="card card-default">
        <div class="card-header">
            <h3 class="card-title">Neraca Lajur Bulanan</h3>

            <div class="card-tools">
                <!-- <button type="button" class="btn btn-tool" data-card-widget="collapse"><i class="fas fa-minus"></i></button> -->
                <!-- <button type="button" class="btn btn-tool" data-card-widget="remove"><i class="fas fa-times"></i></button> -->
            </div>
        </div>
        <div class="card-body">
            <form class="form-horizontal" action="" method="post">
                <div class="card-body">
                    <div class="row">
                        <div class="col-md-3">
                            <div class="form-group">
                                <label>Tahun</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="far fa-calendar-alt"></i>
                                        </span>
                                    </div>
                                    <select name="tahun" id="tahun" class="form-control select2bs4">
                                        <option value="">Pilih Tahun</option>
                                        <?php for ($i = date('Y'); $i >= 2015; $i--) {
                                            if ($_POST['tahun'] == $i)
                                                echo "<option value='" . $i . "' selected>" . $i . "</option>";
                                            else
                                                echo "<option value='" . $i . "'>" . $i . "</option>";
                                        } ?>
                                    </select>
                                    <button type="submit" name="filter" class="btn btn-sm btn-primary btn-flat"><i class="fa fa-search"></i> TAMPILKAN</button>
                                </div>
                            </div>
                        </div>
                        <!-- <div class="col-md-6">
                            <div class="form-group">
                                <label>Tanggal Awal</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="far fa-calendar-alt"></i>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control form-control-sm datepicker-input tanggal" name="tgl1" id="tgl1" data-toggle="datetimepicker" data-target="#datetimepicker" placeholder="YYYY-MM-DD" autocomplete="off">
                                </div>
                            </div>
                        </div>
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Tanggal Akhir</label>
                                <div class="input-group">
                                    <div class="input-group-prepend">
                                        <span class="input-group-text">
                                            <i class="far fa-calendar-alt"></i>
                                        </span>
                                    </div>
                                    <input type="text" class="form-control form-control-sm datepicker-input tanggal" name="tgl2" id="tgl2" data-toggle="datetimepicker" data-target="#datetimepicker" placeholder="YYYY-MM-DD" autocomplete="off">

                                </div>
                            </div> 
                    </div>-->
                    </div>
                </div>
                <!-- <div class="box-footer">
                    <div class="text-center">
                        <button type="submit" name="filter" class="btn btn-sm btn-primary btn-flat"><i class="fa fa-search"></i> TAMPILKAN</button>
                    </div>
                </div> -->
        </div>
        <style>
            .kecilkan {
                padding-top: 0px;
                padding-bottom: 0px;
            }

            td.bg-realisasi {
                background-color: #effdfd;
                color: black;
            }
        </style>
        <?php if (!empty($_POST['tahun'])) { ?>
            <div class="row">
                <div class="col-md-12">
                    <div class="table-responsive">
                        <table class="table table-hovered table-bordered table-sm" id="table">
                            <thead>
                                <tr class="bg-blue">
                                    <th rowspan="2" class="text-center" style="vertical-align: middle;" width="5%">NO</th>
                                    <th rowspan="2" class="text-center" style="vertical-align: middle;" width="10%">KODE</th>
                                    <th rowspan="2" class="text-center" style="vertical-align: middle;">NAMA REKENING</th>
                                    <th colspan="3" class="text-center">JANUARI</th>
                                    <th colspan="3" class="text-center">FEBRUARI</th>
                                    <th colspan="3" class="text-center">MARET</th>
                                    <th colspan="3" class="text-center">APRIL</th>
                                    <th colspan="3" class="text-center">MEI</th>
                                    <th colspan="3" class="text-center">JUNI</th>
                                    <th colspan="3" class="text-center">JULI</th>
                                    <th colspan="3" class="text-center">AGUSTUS</th>
                                    <th colspan="3" class="text-center">SEPTEMBER</th>
                                    <th colspan="3" class="text-center">OKTOBER</th>
                                    <th colspan="3" class="text-center">NOVEMBER</th>
                                    <th colspan="3" class="text-center">DESEMBER</th>
                                    <th colspan="3" class="text-center" width="10%">TOTAL</th>
                                </tr>
                                <tr class="bg-blue">
                                    <th class="text-center">DEBET</th>
                                    <th class="text-center">KREDIT</th>
                                    <th class="text-center">SALDO</th>
                                    <th class="text-center">DEBET</th>
                                    <th class="text-center">KREDIT</th>
                                    <th class="text-center">SALDO</th>
                                    <th class="text-center">DEBET</th>
                                    <th class="text-center">KREDIT</th>
                                    <th class="text-center">SALDO</th>
                                    <th class="text-center">DEBET</th>
                                    <th class="text-center">KREDIT</th>
                                    <th class="text-center">SALDO</th>
                                    <th class="text-center">DEBET</th>
                                    <th class="text-center">KREDIT</th>
                                    <th class="text-center">SALDO</th>
                                    <th class="text-center">DEBET</th>
                                    <th class="text-center">KREDIT</th>
                                    <th class="text-center">SALDO</th>
                                    <th class="text-center">DEBET</th>
                                    <th class="text-center">KREDIT</th>
                                    <th class="text-center">SALDO</th>
                                    <th class="text-center">DEBET</th>
                                    <th class="text-center">KREDIT</th>
                                    <th class="text-center">SALDO</th>
                                    <th class="text-center">DEBET</th>
                                    <th class="text-center">KREDIT</th>
                                    <th class="text-center">SALDO</th>
                                    <th class="text-center">DEBET</th>
                                    <th class="text-center">KREDIT</th>
                                    <th class="text-center">SALDO</th>
                                    <th class="text-center">DEBET</th>
                                    <th class="text-center">KREDIT</th>
                                    <th class="text-center">SALDO</th>
                                    <th class="text-center">DEBET</th>
                                    <th class="text-center">KREDIT</th>
                                    <th class="text-center">SALDO</th>
                                    <th class="text-center">DEBET</th>
                                    <th class="text-center">KREDIT</th>
                                    <th class="text-center">SALDO</th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $n = 0;
                                $total_saldo11 = 0;
                                $rekening11 = "SELECT
                                                    `j`.`id_divisi` AS `id_divisi`,
                                                    `k`.`idrek4`    AS `idrek4`,
                                                    `k`.`kd_rek`    AS `kd_rek`,
                                                    `k`.`namarek4`  AS `namarek4`,

                                                    SUM(IF(`debet` != 0 AND MONTH(tanggal)=1, jumlah, 0)) AS debet_1,
                                                    SUM(IF(`kredit` != 0 AND MONTH(tanggal)=1, jumlah, 0)) AS kredit_1,
                                                    SUM(IF(`debet` != 0 AND MONTH(tanggal)=1, jumlah, 0)) - SUM(IF(`kredit` != 0 AND MONTH(tanggal)=1, jumlah, 0)) AS saldo_1,
                                                    
                                                    SUM(IF(`debet` != 0 AND MONTH(tanggal)=2, jumlah, 0)) AS debet_2,
                                                    SUM(IF(`kredit` != 0 AND MONTH(tanggal)=2, jumlah, 0)) AS kredit_2,
                                                    SUM(IF(`debet` != 0 AND MONTH(tanggal)=2, jumlah, 0)) - SUM(IF(`kredit` != 0 AND MONTH(tanggal)=2, jumlah, 0)) AS saldo_2,
                                                    
                                                    SUM(IF(`debet` != 0 AND MONTH(tanggal)=3, jumlah, 0)) AS debet_3,
                                                    SUM(IF(`kredit` != 0 AND MONTH(tanggal)=3, jumlah, 0)) AS kredit_3,
                                                    SUM(IF(`debet` != 0 AND MONTH(tanggal)=3, jumlah, 0)) - SUM(IF(`kredit` != 0 AND MONTH(tanggal)=3, jumlah, 0)) AS saldo_3,
                                                    
                                                    SUM(IF(`debet` != 0 AND MONTH(tanggal)=4, jumlah, 0)) AS debet_4,
                                                    SUM(IF(`kredit` != 0 AND MONTH(tanggal)=4, jumlah, 0)) AS kredit_4,
                                                    SUM(IF(`debet` != 0 AND MONTH(tanggal)=4, jumlah, 0)) - SUM(IF(`kredit` != 0 AND MONTH(tanggal)=4, jumlah, 0)) AS saldo_4,
                                                    
                                                    SUM(IF(`debet` != 0 AND MONTH(tanggal)=5, jumlah, 0)) AS debet_5,
                                                    SUM(IF(`kredit` != 0 AND MONTH(tanggal)=5, jumlah, 0)) AS kredit_5,
                                                    SUM(IF(`debet` != 0 AND MONTH(tanggal)=5, jumlah, 0)) - SUM(IF(`kredit` != 0 AND MONTH(tanggal)=5, jumlah, 0)) AS saldo_5,
                                                    
                                                    SUM(IF(`debet` != 0 AND MONTH(tanggal)=6, jumlah, 0)) AS debet_6,
                                                    SUM(IF(`kredit` != 0 AND MONTH(tanggal)=6, jumlah, 0)) AS kredit_6,
                                                    SUM(IF(`debet` != 0 AND MONTH(tanggal)=6, jumlah, 0)) - SUM(IF(`kredit` != 0 AND MONTH(tanggal)=6, jumlah, 0)) AS saldo_6,
                                                    
                                                    SUM(IF(`debet` != 0 AND MONTH(tanggal)=7, jumlah, 0)) AS debet_7,
                                                    SUM(IF(`kredit` != 0 AND MONTH(tanggal)=7, jumlah, 0)) AS kredit_7,
                                                    SUM(IF(`debet` != 0 AND MONTH(tanggal)=7, jumlah, 0)) - SUM(IF(`kredit` != 0 AND MONTH(tanggal)=7, jumlah, 0)) AS saldo_7,
                                                    
                                                    SUM(IF(`debet` != 0 AND MONTH(tanggal)=8, jumlah, 0)) AS debet_8,
                                                    SUM(IF(`kredit` != 0 AND MONTH(tanggal)=8, jumlah, 0)) AS kredit_8,
                                                    SUM(IF(`debet` != 0 AND MONTH(tanggal)=8, jumlah, 0)) - SUM(IF(`kredit` != 0 AND MONTH(tanggal)=8, jumlah, 0)) AS saldo_8,
                                                    
                                                    SUM(IF(`debet` != 0 AND MONTH(tanggal)=9, jumlah, 0)) AS debet_9,
                                                    SUM(IF(`kredit` != 0 AND MONTH(tanggal)=9, jumlah, 0)) AS kredit_9,
                                                    SUM(IF(`debet` != 0 AND MONTH(tanggal)=9, jumlah, 0)) - SUM(IF(`kredit` != 0 AND MONTH(tanggal)=9, jumlah, 0)) AS saldo_9,
                                                    
                                                    SUM(IF(`debet` != 0 AND MONTH(tanggal)=10, jumlah, 0)) AS debet_10,
                                                    SUM(IF(`kredit` != 0 AND MONTH(tanggal)=10, jumlah, 0)) AS kredit_10,
                                                    SUM(IF(`debet` != 0 AND MONTH(tanggal)=10, jumlah, 0)) - SUM(IF(`kredit` != 0 AND MONTH(tanggal)=10, jumlah, 0)) AS saldo_10,
                                                    
                                                    SUM(IF(`debet` != 0 AND MONTH(tanggal)=11, jumlah, 0)) AS debet_11,
                                                    SUM(IF(`kredit` != 0 AND MONTH(tanggal)=11, jumlah, 0)) AS kredit_11,
                                                    SUM(IF(`debet` != 0 AND MONTH(tanggal)=11, jumlah, 0)) - SUM(IF(`kredit` != 0 AND MONTH(tanggal)=11, jumlah, 0)) AS saldo_11,
                                                    
                                                    SUM(IF(`debet` != 0 AND MONTH(tanggal)=12, jumlah, 0)) AS debet_12,
                                                    SUM(IF(`kredit` != 0 AND MONTH(tanggal)=12, jumlah, 0)) AS kredit_12,
                                                    SUM(IF(`debet` != 0 AND MONTH(tanggal)=12, jumlah, 0)) - SUM(IF(`kredit` != 0 AND MONTH(tanggal)=12, jumlah, 0)) AS saldo_12,
                                                        
                                                    SUM(IF((`k`.`idrek4` = `j`.`debet`),`j`.`jumlah`,0)) AS `debet_total`,
                                                    SUM(IF((`k`.`idrek4` = `j`.`kredit`),`j`.`jumlah`,0)) AS `kredit_total`,
                                                    (SUM(IF((`k`.`idrek4` = `j`.`debet`),`j`.`jumlah`,0)) - SUM(IF((`k`.`idrek4` = `j`.`kredit`),`j`.`jumlah`,0))) AS `saldo_total`
                                                    FROM (`tb_jurnal` `j`
                                                    JOIN `v_rekening` `k`
                                                                    ON (((`k`.`idrek4` = `j`.`debet`)
                                                                                    OR (`k`.`idrek4` = `j`.`kredit`))))
                                                    WHERE YEAR(`j`.`tanggal`)='$tahun'
                                                    GROUP BY `j`.`idrek4`
                                                    ORDER BY `k`.`kd_rek`
                                            ";
                                $no = 0;
                                $totalrekening11 = 0;
                                $debet_1 = 0;
                                $kredit_1 = 0;
                                $saldo_1 = 0;
                                $debet_2 = 0;
                                $kredit_2 = 0;
                                $saldo_2 = 0;
                                $debet_3 = 0;
                                $kredit_3 = 0;
                                $saldo_3 = 0;
                                $debet_4 = 0;
                                $kredit_4 = 0;
                                $saldo_4 = 0;
                                $debet_5 = 0;
                                $kredit_5 = 0;
                                $saldo_5 = 0;
                                $debet_6 = 0;
                                $kredit_6 = 0;
                                $saldo_6 = 0;
                                $debet_7 = 0;
                                $kredit_7 = 0;
                                $saldo_7 = 0;
                                $debet_8 = 0;
                                $kredit_8 = 0;
                                $saldo_8 = 0;
                                $debet_9 = 0;
                                $kredit_9 = 0;
                                $saldo_9 = 0;
                                $debet_10 = 0;
                                $kredit_10 = 0;
                                $saldo_10 = 0;
                                $debet_11 = 0;
                                $kredit_11 = 0;
                                $saldo_11 = 0;
                                $debet_12 = 0;
                                $kredit_12 = 0;
                                $saldo_12 = 0;
                                $totaldebet = 0;
                                $totalkredit = 0;
                                $totalsaldo = 0;
                                $qrekening11 = mysql_query($rekening11);
                                while ($key11 = mysql_fetch_object($qrekening11)) {
                                    $n++;
                                    $debet_1 = $debet_1 + $key11->debet_1;
                                    $kredit_1 = $kredit_1 + $key11->kredit_1;
                                    $saldo_1 = $saldo_1 + $key11->saldo_1;
                                    $debet_2 = $debet_2 + $key11->debet_2;
                                    $kredit_2 = $kredit_2 + $key11->kredit_2;
                                    $saldo_2 = $saldo_2 + $key11->saldo_2;
                                    $debet_3 = $debet_3 + $key11->debet_3;
                                    $kredit_3 = $kredit_3 + $key11->kredit_3;
                                    $saldo_3 = $saldo_3 + $key11->saldo_3;
                                    $debet_4 = $debet_4 + $key11->debet_4;
                                    $kredit_4 = $kredit_4 + $key11->kredit_4;
                                    $saldo_4 = $saldo_4 + $key11->saldo_4;
                                    $debet_5 = $debet_5 + $key11->debet_5;
                                    $kredit_5 = $kredit_5 + $key11->kredit_5;
                                    $saldo_5 = $saldo_5 + $key11->saldo_5;
                                    $debet_6 = $debet_6 + $key11->debet_6;
                                    $kredit_6 = $kredit_6 + $key11->kredit_6;
                                    $saldo_6 = $saldo_6 + $key11->saldo_6;
                                    $debet_7 = $debet_7 + $key11->debet_7;
                                    $kredit_7 = $kredit_7 + $key11->kredit_7;
                                    $saldo_7 = $saldo_7 + $key11->saldo_7;
                                    $debet_8 = $debet_8 + $key11->debet_8;
                                    $kredit_8 = $kredit_8 + $key11->kredit_8;
                                    $saldo_8 = $saldo_8 + $key11->saldo_8;
                                    $debet_9 = $debet_9 + $key11->debet_9;
                                    $kredit_9 = $kredit_9 + $key11->kredit_9;
                                    $saldo_9 = $saldo_9 + $key11->saldo_9;
                                    $debet_10 = $debet_10 + $key11->debet_10;
                                    $kredit_10 = $kredit_10 + $key11->kredit_10;
                                    $saldo_10 = $saldo_10 + $key11->saldo_10;
                                    $debet_11 = $debet_11 + $key11->debet_11;
                                    $kredit_11 = $kredit_11 + $key11->kredit_11;
                                    $saldo_11 = $saldo_11 + $key11->saldo_11;
                                    $debet_12 = $debet_12 + $key11->debet_12;
                                    $kredit_12 = $kredit_12 + $key11->kredit_12;
                                    $saldo_12 = $saldo_12 + $key11->saldo_12;
                                    $totaldebet = $totaldebet + $key11->debet_total;
                                    $totalkredit = $totalkredit + $key11->kredit_total;
                                    $totalsaldo = $totalsaldo + $key11->saldo_total;
                                ?>
                                    <tr>
                                        <td><?= "<div style='text-align:center;'>" . $n . "<div>"; ?></td>
                                        <td style="white-space: nowrap;"><?= $key11->kd_rek; ?></td>
                                        <td style="white-space: nowrap;"><?= $key11->namarek4; ?></td>
                                        <td class="text-right"><?= number_format($key11->debet_1); ?></td>
                                        <td class="text-right"><?= number_format($key11->kredit_1); ?></td>
                                        <td class="text-right bg-realisasi"><?= number_format($key11->saldo_1); ?></td>
                                        <td class="text-right"><?= number_format($key11->debet_2); ?></td>
                                        <td class="text-right"><?= number_format($key11->kredit_2); ?></td>
                                        <td class="text-right bg-realisasi"><?= number_format($key11->saldo_2); ?></td>
                                        <td class="text-right"><?= number_format($key11->debet_3); ?></td>
                                        <td class="text-right"><?= number_format($key11->kredit_3); ?></td>
                                        <td class="text-right bg-realisasi"><?= number_format($key11->saldo_3); ?></td>
                                        <td class="text-right"><?= number_format($key11->debet_4); ?></td>
                                        <td class="text-right"><?= number_format($key11->kredit_4); ?></td>
                                        <td class="text-right bg-realisasi"><?= number_format($key11->saldo_4); ?></td>
                                        <td class="text-right"><?= number_format($key11->debet_5); ?></td>
                                        <td class="text-right"><?= number_format($key11->kredit_5); ?></td>
                                        <td class="text-right bg-realisasi"><?= number_format($key11->saldo_5); ?></td>
                                        <td class="text-right"><?= number_format($key11->debet_6); ?></td>
                                        <td class="text-right"><?= number_format($key11->kredit_6); ?></td>
                                        <td class="text-right bg-realisasi"><?= number_format($key11->saldo_6); ?></td>
                                        <td class="text-right"><?= number_format($key11->debet_7); ?></td>
                                        <td class="text-right"><?= number_format($key11->kredit_7); ?></td>
                                        <td class="text-right bg-realisasi"><?= number_format($key11->saldo_7); ?></td>
                                        <td class="text-right"><?= number_format($key11->debet_8); ?></td>
                                        <td class="text-right"><?= number_format($key11->kredit_8); ?></td>
                                        <td class="text-right bg-realisasi"><?= number_format($key11->saldo_8); ?></td>
                                        <td class="text-right"><?= number_format($key11->debet_9); ?></td>
                                        <td class="text-right"><?= number_format($key11->kredit_9); ?></td>
                                        <td class="text-right bg-realisasi"><?= number_format($key11->saldo_9); ?></td>
                                        <td class="text-right"><?= number_format($key11->debet_10); ?></td>
                                        <td class="text-right"><?= number_format($key11->kredit_10); ?></td>
                                        <td class="text-right bg-realisasi"><?= number_format($key11->saldo_10); ?></td>
                                        <td class="text-right"><?= number_format($key11->debet_11); ?></td>
                                        <td class="text-right"><?= number_format($key11->kredit_11); ?></td>
                                        <td class="text-right bg-realisasi"><?= number_format($key11->saldo_11); ?></td>
                                        <td class="text-right"><?= number_format($key11->debet_12); ?></td>
                                        <td class="text-right"><?= number_format($key11->kredit_12); ?></td>
                                        <td class="text-right bg-realisasi"><?= number_format($key11->saldo_12); ?></td>
                                        <td class="text-right"><?= number_format($key11->debet_total); ?></td>
                                        <td class="text-right"><?= number_format($key11->kredit_total); ?></td>
                                        <td class="text-right bg-realisasi"><?= number_format($key11->saldo_total); ?></td>
                                    </tr>
                                <?php
                                    $total_saldo11 = $total_saldo11 + $key11->saldo;
                                }
                                $total = $total + $total_saldo11;
                                ?>
                                <tr class="bg-gray">
                                    <th class="text-center" colspan="3">TOTAL:</th>
                                    <th class="text-right"><?= number_format($debet_1) ?></th>
                                    <th class="text-right"><?= number_format($kredit_1) ?></th>
                                    <th class="text-right"><?= number_format($saldo_1) ?></th>
                                    <th class="text-right"><?= number_format($debet_2) ?></th>
                                    <th class="text-right"><?= number_format($kredit_2) ?></th>
                                    <th class="text-right"><?= number_format($saldo_2) ?></th>
                                    <th class="text-right"><?= number_format($debet_3) ?></th>
                                    <th class="text-right"><?= number_format($kredit_3) ?></th>
                                    <th class="text-right"><?= number_format($saldo_3) ?></th>
                                    <th class="text-right"><?= number_format($debet_4) ?></th>
                                    <th class="text-right"><?= number_format($kredit_4) ?></th>
                                    <th class="text-right"><?= number_format($saldo_4) ?></th>
                                    <th class="text-right"><?= number_format($debet_5) ?></th>
                                    <th class="text-right"><?= number_format($kredit_5) ?></th>
                                    <th class="text-right"><?= number_format($saldo_5) ?></th>
                                    <th class="text-right"><?= number_format($debet_6) ?></th>
                                    <th class="text-right"><?= number_format($kredit_6) ?></th>
                                    <th class="text-right"><?= number_format($saldo_6) ?></th>
                                    <th class="text-right"><?= number_format($debet_7) ?></th>
                                    <th class="text-right"><?= number_format($kredit_7) ?></th>
                                    <th class="text-right"><?= number_format($saldo_7) ?></th>
                                    <th class="text-right"><?= number_format($debet_8) ?></th>
                                    <th class="text-right"><?= number_format($kredit_8) ?></th>
                                    <th class="text-right"><?= number_format($saldo_8) ?></th>
                                    <th class="text-right"><?= number_format($debet_9) ?></th>
                                    <th class="text-right"><?= number_format($kredit_9) ?></th>
                                    <th class="text-right"><?= number_format($saldo_9) ?></th>
                                    <th class="text-right"><?= number_format($debet_10) ?></th>
                                    <th class="text-right"><?= number_format($kredit_10) ?></th>
                                    <th class="text-right"><?= number_format($saldo_10) ?></th>
                                    <th class="text-right"><?= number_format($debet_11) ?></th>
                                    <th class="text-right"><?= number_format($kredit_11) ?></th>
                                    <th class="text-right"><?= number_format($saldo_11) ?></th>
                                    <th class="text-right"><?= number_format($debet_12) ?></th>
                                    <th class="text-right"><?= number_format($kredit_12) ?></th>
                                    <th class="text-right"><?= number_format($saldo_12) ?></th>
                                    <th class="text-right"><?= number_format($totaldebet) ?></th>
                                    <th class="text-right"><?= number_format($totalkredit) ?></th>
                                    <th class="text-right"><?= number_format($totalsaldo) ?></th>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        <?php } ?>
        </form>
    </div>
</div>