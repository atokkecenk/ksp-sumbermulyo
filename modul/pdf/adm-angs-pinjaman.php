<?php
require('../../db.php');
// require('../../plugins/fpdf/html_table.php');
require('../../plugins/fpdf/fpdf.php');

function penyebut($nilai)
{
    $nilai = abs($nilai);
    $huruf = array("", "satu", "dua", "tiga", "empat", "lima", "enam", "tujuh", "delapan", "sembilan", "sepuluh", "sebelas");
    $temp = "";
    if ($nilai < 12) {
        $temp = " " . $huruf[$nilai];
    } else if ($nilai < 20) {
        $temp = penyebut($nilai - 10) . " belas";
    } else if ($nilai < 100) {
        $temp = penyebut($nilai / 10) . " puluh" . penyebut($nilai % 10);
    } else if ($nilai < 200) {
        $temp = " seratus" . penyebut($nilai - 100);
    } else if ($nilai < 1000) {
        $temp = penyebut($nilai / 100) . " ratus" . penyebut($nilai % 100);
    } else if ($nilai < 2000) {
        $temp = " seribu" . penyebut($nilai - 1000);
    } else if ($nilai < 1000000) {
        $temp = penyebut($nilai / 1000) . " ribu" . penyebut($nilai % 1000);
    } else if ($nilai < 1000000000) {
        $temp = penyebut($nilai / 1000000) . " juta" . penyebut($nilai % 1000000);
    } else if ($nilai < 1000000000000) {
        $temp = penyebut($nilai / 1000000000) . " milyar" . penyebut(fmod($nilai, 1000000000));
    } else if ($nilai < 1000000000000000) {
        $temp = penyebut($nilai / 1000000000000) . " trilyun" . penyebut(fmod($nilai, 1000000000000));
    }
    return $temp;
}

function terbilang($nilai)
{
    if ($nilai < 0) {
        $hasil = "minus " . trim(penyebut($nilai));
    } else {
        $hasil = trim(penyebut($nilai));
    }
    return $hasil;
}

function rupiah($nilai)
{
    return number_format($nilai, 0, ',', '.');
}

$data_angsuran = mysql_fetch_array(mysql_query("SELECT 
a.kode,
a.nama,
a.alamat,
r.no_rekening,
m.jml_angsuran + sisa_hutang as saldo_lalu,
m.jml_angsuran,
m.saldo,
m.tanggal,
m.no_mutasi
FROM tb_mutasi m 
JOIN tb_rekening r on r.id_rekening = m.id_rekening
JOIN tb_anggota a on a.id = r.id_anggota
where m.id_mutasi = '$_GET[id]'"));

$pdf = new FPDF('L', 'mm', [148, 210]);
$pdf->SetMargins(8, 12, 8);
$pdf->SetAutoPageBreak(true);

// Slip ADM pinjaman
$pdf->AddPage();

$pdf->Image('../../dist/img/icon/koperasi-logo.png', 15.5, 13.5, 22, 22);

$pdf->Cell(192, 3, '', 'LTR', 0, 'C');
$pdf->Ln();
$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(30, 6, '', 'L', 0, 'L');
$pdf->Cell(81, 6, 'KOPERASI SUMBER MULYO', 0, 0, 'C');
$pdf->SetFont('Arial', 'BU', 12);
$pdf->Cell(81, 6, 'TANDA SETORAN', 'R', 0, 'C');
$pdf->Ln();
$pdf->SetFont('Arial', '', 8);
$pdf->Cell(30, 4, '', 'L', 0, 'L');
$pdf->Cell(81, 4, 'BH. NO518/007.BH/XVI.7/410/2013', 0, 0, 'C');
$pdf->SetFont('Arial', 'B', 8);
$pdf->Cell(81, 4, 'No. Bukti Trans. : ' . $data_angsuran['no_mutasi'], 'R', 0, 'C');
$pdf->Ln();
$pdf->SetFont('Arial', '', 8);
$pdf->Cell(30, 4, '', 'L', 0, 'L');
$pdf->Cell(81, 4, '28 Oktober 2013', 0, 0, 'C');
$pdf->SetFont('Arial', 'B', 8);
$pdf->Cell(81, 4, '', 'R', 0, 'C');
$pdf->Ln();
$pdf->SetFont('Arial', '', 8);
$pdf->Cell(30, 4, '', 'L', 0, 'L');
$pdf->Cell(81, 4, 'TEGALSARI - AMBULU - JEMBER', 0, 0, 'C');
$pdf->SetFont('Arial', 'B', 8);
$pdf->Cell(81, 4, 'No.Kredit : ' . $data_angsuran['no_rekening'], 'R', 0, 'C');
$pdf->Ln();
$pdf->Cell(192, 4, '', 'LBR', 0, 'C');
$pdf->Ln();

$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(110, 8, '  TELAH DITERIMA SETORAN DARI', 'LT', 0, 'L');
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(21, 8, 'Keterangan', 'T', 0, 'L');
$pdf->Cell(3, 8, ' : ', 'T', 0, 'C');
$pdf->Cell(58, 8, date('d/m/Y') . '  s/d  ' . date('d/m/Y'), 'TR', 0, 'C');
$pdf->Ln();
$pdf->Cell(10, 7, '', 'L', 0, 'L');
$pdf->Cell(30, 7, 'No. Anggt/Cln', 0, 0, 'L');
$pdf->Cell(70, 7, ': ' . $data_angsuran['kode'], 0, 0, 'L');
$pdf->Cell(40, 7, 'Sisa Pokok Bln Lalu', 0, 0, 'L');
$pdf->Cell(8, 7, ': Rp', 0, 0, 'L');
$pdf->Cell(28, 7, rupiah($data_angsuran['saldo_lalu']), 0, 0, 'R');
$pdf->Cell(6, 7, '', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell(10, 7, '', 'L', 0, 'L');
$pdf->Cell(30, 7, 'Nama', 0, 0, 'L');
$pdf->Cell(70, 7, ': ' . $data_angsuran['nama'], 0, 0, 'L');
$pdf->Cell(40, 7, 'Angs. Pokok Bln ini ke 7', 0, 0, 'L');
$pdf->Cell(8, 7, ': Rp', 0, 0, 'L');
$pdf->Cell(28, 7, rupiah($data_angsuran['jml_angsuran']), 0, 0, 'R');
$pdf->Cell(6, 7, '', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell(10, 7, '', 'L', 0, 'L');
$pdf->Cell(30, 7, 'Alamat', 0, 0, 'L');
$pdf->Cell(70, 7, ': ' . $data_angsuran['alamat'], 0, 0, 'L');
$pdf->Cell(40, 7, 'Sisa Pokok Bln ini', 0, 0, 'L');
$pdf->Cell(8, 7, ': Rp', 0, 0, 'L');
$pdf->Cell(28, 7, rupiah($data_angsuran['saldo']), 0, 0, 'R');
$pdf->Cell(6, 7, '', 'R', 0, 'L');
$pdf->Ln();

$pdf->Cell(192, 6, '', 'LR', 0, 'L');
$pdf->Ln();
$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(192, 8, '  JENIS SETORAN', 'LR', 0, 'L');
$pdf->SetFont('Arial', '', 10);
$pdf->Ln();
$pdf->Cell(10, 7, '', 'L', 0, 'L');
$pdf->Cell(30, 7, 'Angs. Pokok', 0, 0, 'L');
$pdf->Cell(8, 7, ': Rp', 0, 0, 'L');
$pdf->Cell(40, 7, 'xxx', 0, 0, 'R');
$pdf->Cell(10, 7, '', 0, 0, 'L');
$pdf->SetFont('Arial', 'I', 9);
$pdf->Cell(90, 7, 'Terbilang : ', 'LTR', 0, 'L');
$pdf->Cell(4, 7, '', 'R', 0, 'L');
$pdf->Ln();
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(10, 7, '', 'L', 0, 'L');
$pdf->Cell(30, 7, 'Jasa', 0, 0, 'L');
$pdf->Cell(8, 7, ': Rp', 0, 0, 'L');
$pdf->Cell(40, 7, 'xxx', 0, 0, 'R');
$pdf->Cell(10, 7, '', 0, 0, 'L');
$pdf->SetFont('Arial', 'I', 9);
$pdf->Cell(90, 7, '', 'LBR', 0, 'L');
$pdf->Cell(4, 7, '', 'R', 0, 'L');
$pdf->Ln();
// $pdf->SetFont('Arial', '', 10);
// $pdf->Cell(10, 7, '', 'L', 0, 'L');
// $pdf->Cell(30, 7, 'Potongan', 0, 0, 'L');
// $pdf->Cell(8, 7, ': ', 0, 0, 'L');
// $pdf->Cell(40, 7, '', 0, 0, 'R');
// $pdf->Cell(100, 7, '', 0, 0, 'L');
// $pdf->Cell(4, 7, '', 'R', 0, 'L');
// $pdf->Ln();

$pdf->SetFont('Arial', '', 10);
$pdf->Cell(10, 7, '', 'L', 0, 'L');
$pdf->Cell(30, 7, 'Potongan', 0, 0, 'L');
$pdf->Cell(8, 7, ': Rp', 0, 0, 'L');
$pdf->Cell(40, 7, '-', 0, 0, 'R');
$pdf->Cell(104, 7, date('d/m/Y') . '                         ', 'R', 0, 'R');
$pdf->Ln(5);
$pdf->Cell(98, 7, '', 0, 0, 'L');
$pdf->Cell(31, 7, 'Petugas', 0, 0, 'C');
$pdf->Cell(31, 7, 'Kasir', 0, 0, 'C');
$pdf->Cell(32, 7, 'Penyetor', 0, 0, 'C');
$pdf->Ln(2);

$pdf->SetLineWidth(0.8);
$pdf->Line(19, 101.5, 97, 101.5);
$pdf->Ln(0);

$pdf->SetLineWidth(0.8);
$pdf->Line(19, 110.5, 97, 110.5);
$pdf->Ln(0);

$pdf->SetLineWidth(0.2);
$pdf->SetFont('Arial', '', 12);
$pdf->Cell(10, 10, '', 'L', 0, 'L');
$pdf->Cell(30, 10, 'JUMLAH', 0, 0, 'L');
$pdf->Cell(8, 10, 'Rp', 0, 0, 'L');
$pdf->Cell(40, 10, 'xxx', 0, 0, 'R');
$pdf->Cell(104, 10, '', 'R', 0, 'R');
$pdf->Ln();

$pdf->SetFont('Arial', '', 9);
$pdf->Cell(100, 7, '   Pembayaran dianggap syah jika sudah dilegalisir oleh kasir', 'L', 0, 'L');
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(26, 7, '', 'B', 0, 'C');
$pdf->Cell(5, 7, '', 0, 0, 'C');
$pdf->Cell(26, 7, '', 'B', 0, 'C');
$pdf->Cell(5, 7, '', 0, 0, 'C');
$pdf->Cell(25, 7, '', 'B', 0, 'C');
$pdf->Cell(5, 7, '', 'R', 0, 'C');
$pdf->Ln();
$pdf->SetFont('Arial', 'I', 8);
$pdf->Cell(140, 6, '', 'LB', 0, 'C');
$pdf->Cell(52, 6, date('d/m/y H:i:s'), 'RB', 0, 'C');
$pdf->Ln();


$pdf->Output();
