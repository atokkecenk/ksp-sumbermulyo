<?php
// require('../../plugins/fpdf/html_table.php');
require('../../plugins/fpdf/fpdf.php');

$pdf = new FPDF('L', 'mm', [148, 210]);
$pdf->SetMargins(8, 12, 8);
$pdf->SetAutoPageBreak(true);

// Slip ADM pinjaman
$pdf->AddPage();

$pdf->Image('../../dist/img/icon/koperasi-logo.png', 15.5, 13.5, 22, 22);

$pdf->Cell(192, 3, '', 'LTR', 0, 'C');
$pdf->Ln();
$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(30, 6, '', 'L', 0, 'L');
$pdf->Cell(81, 6, 'KOPERASI SUMBER MULYO', 0, 0, 'C');
$pdf->SetFont('Arial', 'BU', 12);
$pdf->Cell(81, 6, 'ADMINISTRASI PINJAMAN', 'R', 0, 'C');
$pdf->Ln();
$pdf->SetFont('Arial', '', 8);
$pdf->Cell(30, 4, '', 'L', 0, 'L');
$pdf->Cell(81, 4, 'BH. NO518/007.BH/XVI.7/410/2013', 0, 0, 'C');
$pdf->SetFont('Arial', 'B', 8);
$pdf->Cell(81, 4, 'Nomor :            KR14166', 'R', 0, 'C');
$pdf->Ln();
$pdf->SetFont('Arial', '', 8);
$pdf->Cell(30, 4, '', 'L', 0, 'L');
$pdf->Cell(81, 4, '28 Oktober 2013', 0, 0, 'C');
$pdf->SetFont('Arial', 'B', 8);
$pdf->Cell(81, 4, '', 'R', 0, 'C');
$pdf->Ln();
$pdf->SetFont('Arial', '', 8);
$pdf->Cell(30, 4, '', 'L', 0, 'L');
$pdf->Cell(81, 4, 'TEGALSARI - AMBULU - JEMBER', 0, 0, 'C');
$pdf->SetFont('Arial', 'B', 8);
$pdf->Cell(81, 4, 'No.Kredit : KR14166', 'R', 0, 'C');
$pdf->Ln();
$pdf->Cell(192, 4, '', 'LBR', 0, 'C');
$pdf->Ln();

$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(120, 8, '  HARAP DIBAYARKAN KEPADA', 'LT', 0, 'L');
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(30, 8, 'Avalis', 'T', 0, 'L');
$pdf->Cell(42, 8, ': ', 'TR', 0, 'L');
$pdf->Ln();
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(10, 7, '', 'L', 0, 'L');
$pdf->Cell(30, 7, 'No. Anggt/Cln', 0, 0, 'L');
$pdf->Cell(80, 7, ': ', 0, 0, 'L');
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(30, 7, 'Masa Pelunasan', 0, 0, 'L');
$pdf->Cell(42, 7, ': 4 Bulan', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell(10, 7, '', 'L', 0, 'L');
$pdf->Cell(30, 7, 'Nama', 0, 0, 'L');
$pdf->Cell(152, 7, ':', 'R', 0, 'L');
$pdf->Ln();
$pdf->Cell(10, 7, '', 'L', 0, 'L');
$pdf->Cell(30, 7, 'Alamat', 0, 0, 'L');
$pdf->Cell(152, 7, ':', 'R', 0, 'L');
$pdf->Ln();

$pdf->Cell(192, 6, '', 'LR', 0, 'L');
$pdf->Ln();
$pdf->SetFont('Arial', 'B', 12);
$pdf->Cell(192, 8, '  JENIS PEMBAYARAN', 'LR', 0, 'L');
$pdf->SetFont('Arial', '', 10);
$pdf->Ln();
$pdf->Cell(10, 7, '', 'L', 0, 'L');
$pdf->Cell(30, 7, 'Pokok Pinjaman', 0, 0, 'L');
$pdf->Cell(8, 7, ': Rp', 0, 0, 'L');
$pdf->Cell(40, 7, 'xxx', 0, 0, 'R');
$pdf->Cell(10, 7, '', 0, 0, 'L');
$pdf->SetFont('Arial', 'I', 9);
$pdf->Cell(90, 7, 'Terbilang : ', 'LTR', 0, 'L');
$pdf->Cell(4, 7, '', 'R', 0, 'L');
$pdf->Ln();
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(10, 7, '', 'L', 0, 'L');
$pdf->Cell(30, 7, 'Biaya Administrasi', 0, 0, 'L');
$pdf->Cell(8, 7, ': Rp', 0, 0, 'L');
$pdf->Cell(40, 7, 'xxx', 0, 0, 'R');
$pdf->Cell(10, 7, '', 0, 0, 'L');
$pdf->SetFont('Arial', 'I', 9);
$pdf->Cell(90, 7, '', 'LBR', 0, 'L');
$pdf->Cell(4, 7, '', 'R', 0, 'L');
$pdf->Ln();
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(10, 7, '', 'L', 0, 'L');
$pdf->Cell(30, 7, 'Biaya Provisi', 0, 0, 'L');
$pdf->Cell(8, 7, ': Rp', 0, 0, 'L');
$pdf->Cell(40, 7, 'xxx', 0, 0, 'R');
$pdf->Cell(100, 7, '', 0, 0, 'L');
$pdf->Cell(4, 7, '', 'R', 0, 'L');
$pdf->Ln();

$pdf->SetFont('Arial', '', 10);
$pdf->Cell(10, 7, '', 'L', 0, 'L');
$pdf->Cell(30, 7, 'Biaya Lain', 0, 0, 'L');
$pdf->Cell(8, 7, ': Rp', 0, 0, 'L');
$pdf->Cell(40, 7, '-', 0, 0, 'R');
$pdf->Cell(104, 7, date('d/m/Y') . '                         ', 'R', 0, 'R');
$pdf->Ln(5);
$pdf->Cell(98, 7, '', 0, 0, 'L');
$pdf->Cell(31, 7, 'Ketua', 0, 0, 'C');
$pdf->Cell(31, 7, 'Kasir', 0, 0, 'C');
$pdf->Cell(32, 7, 'Penerima', 0, 0, 'C');
$pdf->Ln(2);

$pdf->SetLineWidth(0.8);
$pdf->Line(19, 109, 97, 109);
$pdf->Ln(0);

$pdf->SetLineWidth(0.8);
$pdf->Line(19, 117.5, 97, 117.5);
$pdf->Ln(0);

$pdf->SetLineWidth(0.2);
$pdf->SetFont('Arial', '', 12);
$pdf->Cell(10, 10, '', 'L', 0, 'L');
$pdf->Cell(30, 10, 'JUMLAH', 0, 0, 'L');
$pdf->Cell(8, 10, 'Rp', 0, 0, 'L');
$pdf->Cell(40, 10, 'xxx', 0, 0, 'R');
$pdf->Cell(104, 10, '', 'R', 0, 'R');
$pdf->Ln();

$pdf->SetFont('Arial', '', 9);
$pdf->Cell(100, 7, '   Pembayaran dianggap syah jika sudah dilegalisir oleh kasir', 'L', 0, 'L');
$pdf->SetFont('Arial', '', 10);
$pdf->Cell(26, 7, '', 'B', 0, 'C');
$pdf->Cell(5, 7, '', 0, 0, 'C');
$pdf->Cell(26, 7, '', 'B', 0, 'C');
$pdf->Cell(5, 7, '', 0, 0, 'C');
$pdf->Cell(25, 7, '', 'B', 0, 'C');
$pdf->Cell(5, 7, '', 'R', 0, 'C');
$pdf->Ln();
$pdf->SetFont('Arial', 'I', 8);
$pdf->Cell(140, 6, '', 'LB', 0, 'C');
$pdf->Cell(52, 6, date('d/m/y H:i:s'), 'RB', 0, 'C');
$pdf->Ln();


$pdf->Output();
