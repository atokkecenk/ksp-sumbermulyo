<?php
// require('../../plugins/fpdf/html_table.php');
require('../../plugins/fpdf/fpdf.php');

$pdf = new FPDF('L', 'mm', [148, 210]);
$pdf->SetMargins(8, 12, 8);
$pdf->SetAutoPageBreak(true);

// Slip ADM pinjaman
$pdf->AddPage();

$pdf->Image('../../dist/img/icon/koperasi-logo-warna.png', 10, 10, 18, 18);

$pdf->Ln();
$pdf->SetFont('Arial', '', 7);
$pdf->Cell(23, 3.5, '', 0, 0, 'L');
$pdf->Cell(56, 3.5, 'KOPERASI SUMBER MULYO', 0, 0, 'C');
$pdf->SetFont('Arial', '', 7);
$pdf->Cell(106, 3.5, 'Lembar 1', 0, 0, 'R');
$pdf->Ln();
$pdf->SetFont('Arial', 'B', 17);
$pdf->Cell(23, 5, '', 0, 0, 'L');
$pdf->Cell(56, 5, 'SUMBER MULYO', 0, 0, 'C');
$pdf->SetFont('Arial', 'B', 7);
$pdf->Cell(88, 5, '', 0, 0, 'R');
$pdf->Ln();
$pdf->SetFont('Arial', '', 7);
$pdf->Cell(23, 3.5, '', 0, 0, 'L');
$pdf->Cell(56, 3.5, 'BADAN HUKUM : NO. 518/007.BH/XVI.7/410/2013', 0, 0, 'C');
$pdf->SetFont('Arial', 'B', 7);
$pdf->Cell(88, 3.5, '', 0, 0, 'R');
$pdf->Ln();
$pdf->SetFont('Arial', '', 7);
$pdf->Cell(23, 3.5, '', 0, 0, 'L');
$pdf->Cell(56, 3.5, 'Jl. Semeru No.20 Tegalsari - Ambulu - Jember', 0, 0, 'C');
$pdf->SetFont('Arial', 'B', 7);
$pdf->Cell(88, 3.5, '', 0, 0, 'R');
$pdf->Ln(5);

$pdf->SetFont('Arial', 'B', 14);
$pdf->Cell(193, 6, 'Surat Tabungan Berjangka', 0, 0, 'C');
$pdf->Ln(8);

$pdf->SetFont('Arial', '', 10);
$pdf->Cell(130, 8, 'Telah dibukukan ke dalam tabungan berjangka :', 0, 0, 'L');
$pdf->Cell(63, 8, 'No. Rek.', 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(42, 8, 'Atas Nama', 0, 0, 'L');
$pdf->Cell(5, 8, ':', 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(42, 8, 'Alamat', 0, 0, 'L');
$pdf->Cell(5, 8, ':', 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(42, 8, 'Jumlah', 0, 0, 'L');
$pdf->Cell(5, 8, ':  Rp.', 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(42, 8, 'Terbilang', 0, 0, 'L');
$pdf->Cell(5, 8, ':', 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(42, 8, 'Suku Bunga', 0, 0, 'L');
$pdf->Cell(5, 8, ':', 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(42, 8, 'Jangka Waktu', 0, 0, 'L');
$pdf->Cell(5, 8, ':', 0, 0, 'L');
$pdf->Ln();
$pdf->Cell(42, 8, 'Tanggal Jatuh Waktu', 0, 0, 'L');
$pdf->Cell(5, 8, ':', 0, 0, 'L');
$pdf->Ln(23);

$pdf->Cell(108, 6, '', 0, 0, 'C');
$pdf->Cell(85, 6, '( ____________________________________ )', 0, 0, 'C');
$pdf->Ln();
$pdf->SetFont('Arial', '', 8);
$pdf->Cell(108, 4.5, '', 0, 0, 'C');
$pdf->Cell(85, 4.5, 'ketentuan mengenai tabungan berjangka pada sisi bilyet ini', 0, 0, 'C');
$pdf->Ln();



$pdf->Output();
