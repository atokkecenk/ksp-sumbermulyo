<?php
$msg = '';
$page = $_GET['page'];
$get = mysql_fetch_array(mysql_query("SELECT * FROM tb_setting WHERE id = 1")) or die(mysql_error());
if (isset($_POST['update'])) {
    $upd = mysql_query("UPDATE tb_setting SET nominal_pendaftaran = '$_POST[nominal]',
                                                ket_pendaftaran = '$_POST[keterangan]',
                                                rek_debet_pendaftaran = '$_POST[ak_debet]',
                                                rek_kredit_pendaftaran = '$_POST[ak_kredit]',
                                                nominal_tutuptabku = '$_POST[nominal_tutuptab]',
                                                rek_debet_tutuptabku = '$_POST[ak_debet_tutuptab]',
                                                rek_kredit_tutuptabku = '$_POST[ak_kredit_tutuptab]',
                                                update_by = '$_SESSION[session_user]',
                                                update_at = NOW()
                                            WHERE id = '1'") or die(mysql_error());

    if ($upd) {
        $msg = 1;
    } else {
        $msg = 0;
    }
    echo "<meta http-equiv='refresh'content='1;url=?page=" . $page . "'> ";
}
?>
<div class="container-fluid">
    <div class="col-12">
        <?php
        if (isset($msg) && $msg !== '') {
            if ($msg == 1) {
                echo '<div class="alert alert-success alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        Berhasil update data.
                    </div>';
            } elseif ($msg == 0) {
                echo '<div class="alert alert-danger alert-dismissible">
                        <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
                        Gagal update data !!
                    </div>';
            }
        }
        ?>
        <div class="card card-info">
            <div class="card-header ui-sortable-handle">
                <h3 class="card-title">Setting</h3>

                <!-- <div class="card-tools">
                    <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Minimize data">
                        <i class="fas fa-minus"></i>
                    </button>
                    <a href="?page=anggota" class="btn btn-tool" title="Tambah Anggota">
                        <i class="fas fa-plus"></i>
                    </a>
                    <button type="button" class="btn btn-tool" data-card-widget="remove" title="Tutup Data">
                        <i class="fas fa-times"></i>
                    </button>
                </div> -->
            </div>
            <form method="post" action="">
                <div class="card-body">
                    <div class="row">
                        <!-- Kiri -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nominal Pendaftaran</label>
                                <input type="text" class="form-control" name="nominal" id="rupiah" value="<?= $get['nominal_pendaftaran'] ?>" placeholder="Rp. 0">
                            </div>
                            <div class="form-group">
                                <label>Keterangan Pendaftaran</label>
                                <textarea class="form-control" rows="2" name="keterangan"><?= $get['ket_pendaftaran'] ?></textarea>
                            </div>
                            <div class="form-group">
                                <label>Rekening Debet Pendaftaran</label>
                                <select class="form-control select2" name="ak_debet" style="width: 100%;">
                                    <option value="">Pilih Rekening</option>
                                    <?php
                                    $sql = mysql_query("SELECT * FROM v_rekening");
                                    while ($rows = mysql_fetch_array($sql)) {
                                        $slc = ($rows['idrek4'] == $get['rek_debet_pendaftaran']) ? 'selected' : null;
                                    ?>
                                        <option value="<?= $rows['idrek4'] ?>" <?= $slc ?>><?= $rows['kd_rek'] . ' - ' . $rows['namarek4'] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Rekening Kredit Pendaftaran</label>
                                <select class="form-control select2" name="ak_kredit" style="width: 100%;">
                                    <option value="">Pilih Rekening</option>
                                    <?php
                                    $sql = mysql_query("SELECT * FROM v_rekening");
                                    while ($rows = mysql_fetch_array($sql)) {
                                        $slc = ($rows['idrek4'] == $get['rek_kredit_pendaftaran']) ? 'selected' : null;
                                    ?>
                                        <option value="<?= $rows['idrek4'] ?>" <?= $slc ?>><?= $rows['kd_rek'] . ' - ' . $rows['namarek4'] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>

                        <!-- Kanan -->
                        <div class="col-md-6">
                            <div class="form-group">
                                <label>Nominal Penutupan Tabunganku</label>
                                <input type="text" class="form-control" name="nominal_tutuptab" id="rupiah" value="<?= $get['nominal_tutuptabku'] ?>" placeholder="Rp. 0">
                            </div>
                            <div class="form-group">
                                <label>Rekening Debet Penutupan Tabunganku</label>
                                <select class="form-control select2" name="ak_debet_tutuptab" style="width: 100%;">
                                    <option value="">Pilih Rekening</option>
                                    <?php
                                    $sql = mysql_query("SELECT * FROM v_rekening");
                                    while ($rows = mysql_fetch_array($sql)) {
                                        $slc = ($rows['idrek4'] == $get['rek_debet_tutuptabku']) ? 'selected' : null;
                                    ?>
                                        <option value="<?= $rows['idrek4'] ?>" <?= $slc ?>><?= $rows['kd_rek'] . ' - ' . $rows['namarek4'] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                            <div class="form-group">
                                <label>Rekening Kredit Penutupan Tabunganku</label>
                                <select class="form-control select2" name="ak_kredit_tutuptab" style="width: 100%;">
                                    <option value="">Pilih Rekening</option>
                                    <?php
                                    $sql = mysql_query("SELECT * FROM v_rekening");
                                    while ($rows = mysql_fetch_array($sql)) {
                                        $slc = ($rows['idrek4'] == $get['rek_kredit_tutuptabku']) ? 'selected' : null;
                                    ?>
                                        <option value="<?= $rows['idrek4'] ?>" <?= $slc ?>><?= $rows['kd_rek'] . ' - ' . $rows['namarek4'] ?></option>
                                    <?php
                                    }
                                    ?>
                                </select>
                            </div>
                        </div>
                    </div>

                    <div class="row">
                        <div class="col-sm-12">
                            <button type="submit" class="btn btn-primary" name="update"><i class="fas fa-save mr-2"></i>Update</button>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    </div>
</div>