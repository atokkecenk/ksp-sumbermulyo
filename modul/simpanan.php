<?php
$jasa = $_POST['jasa'] / 100;
if (isset($_POST['simpan'])) {
    $biaya_admin = hilangTitik($_POST['biaya_admin']);
    $ins = mysql_query("INSERT INTO tb_produksimpanan (
    kode_produk,
    nama_produk,
    jasa,
    biaya_admin,
    tanggal_berlaku,
    id_jangka,
    idrek_debet_setoran,
    idrek_kredit_setoran,
    idrek_debet_penarikan, 
    idrek_kredit_penarikan,
    idrek_debet_jasa,
    idrek_kredit_jasa,
    idrek_debet_administrasi,
    idrek_kredit_administrasi,
    efektif) VALUES (
    '$_POST[kode_produk]',
    '$_POST[nama_produk]',
    '$jasa',
    '$biaya_admin',
    CURDATE(),
    '$_POST[hitung]',
    '$_POST[rekening_debet_setoran]',
    '$_POST[rekening_kredit_setoran]',
    '$_POST[rekening_debet_penarikan]',
    '$_POST[rekening_kredit_penarikan]',
    '$_POST[rekening_debet_jasa]',
    '$_POST[rekening_kredit_jasa]',
    '$_POST[rekening_debet_admin]',
    '$_POST[rekening_kredit_admin]',
    '$_POST[efektif]'
    )");

    $id_produk = mysql_insert_id();
    $ins = mysql_query("INSERT INTO tb_produksimpanan_histori(id_produk,jasa,biaya_admin,tanggal_berlaku) VALUES ('$id_produk','$jasa','$_POST[biaya_admin]',CURDATE())");

    if ($ins) {
        echo "<script>
        document.location=\"?page=simpanan\"
        </script>";
    } else {
        echo "<script>
        alert(\"Gagal\")
        document.location=\"?page=simpanan\"
        </script>";
    }
}

if (isset($_POST["simpanedit1"])) {
    $edt = mysql_query("UPDATE tb_produksimpanan SET 
    idrek_debet_setoran = '$_POST[rekening_debet_setoran]',
    idrek_kredit_setoran = '$_POST[rekening_kredit_setoran]',
    idrek_debet_penarikan = '$_POST[rekening_debet_penarikan]',
    idrek_kredit_penarikan = '$_POST[rekening_kredit_penarikan]',
    idrek_debet_jasa = '$_POST[rekening_debet_jasa]',
    idrek_kredit_jasa = '$_POST[rekening_kredit_jasa]',
    idrek_debet_administrasi = '$_POST[rekening_debet_admin]',
    idrek_kredit_administrasi = '$_POST[rekening_kredit_admin]'
    WHERE sha1(id_produk) = '$_GET[id]'");

    if ($edt) {
        echo "<script>
        document.location=\"?page=simpanan\"
        </script>";
    } else {
        echo "<script>
        alert(\"Gagal\")
        document.location=\"?page=simpanan\"
        </script>";
    }
}

if (isset($_POST["simpanedit2"])) {
    $jasa = $_POST['jasa'] / 100;
    $biaya_admin = hilangTitik($_POST['biaya_admin']);
    $data_produk = mysql_fetch_array(mysql_query("SELECT * FROM tb_produksimpanan where sha1(id_produk) = '$_GET[id]'"));
    $edt = mysql_query("UPDATE tb_produksimpanan SET jasa = '$jasa', biaya_admin = '$biaya_admin', tanggal_berlaku = '$_POST[tanggal]' where sha1(id_produk) = '$_GET[id]'");
    $edt = mysql_query("INSERT INTO tb_produksimpanan_histori(id_produk,jasa,biaya_admin,tanggal_berlaku) VALUES ('$data_produk[id_produk]','$jasa','$_POST[biaya_admin]','$_POST[tanggal]')");
    if ($edt) {
        echo "<script>
        document.location=\"?page=simpanan\"
        </script>";
    } else {
        echo "<script>
        alert(\"Gagal\")
        document.location=\"?page=simpanan\"
        </script>";
    }
}

switch ($_GET['act']) {
    case 'edt2':
        $data_edit = mysql_fetch_array(mysql_query("SELECT * from tb_produksimpanan s JOIN tb_metodehitungjasa m on m.id = s.id_jangka where sha1(s.id_produk) = '$_GET[id]'"));
        if ($data_edit['efektif'] == 'Y') {
            $s_efektif = "Efektif";
        } else {
            $s_efektif = "";
        }
?>
        <div class="container-fluid">
            <form action="" method="POST">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Detail Produk</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="kode_produk">Kode Produk</label>
                                    <input type="text" class="form-control form-control-sm" name="kode_produk" id="kode_produk" value="<?= $data_edit['kode_produk'] ?>" placeholder="Kode Produk">
                                </div>
                                <div class="form-group">
                                    <label for="nama_produk">Nama Produk</label>
                                    <input type="text" class="form-control form-control-sm" name="nama_produk" id="nama_produk" value="<?= $data_edit['nama_produk'] ?>" placeholder="Nama Produk">
                                </div>
                                <div class="form-group">
                                    <label for="nama_produk">Jasa Produk</label>
                                    <div class="input-group">
                                        <input type="text" name="jasa" class="form-control" value="<?= ($data_edit['jasa'] * 100); ?>">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><b>% / <?= $data_edit['nama'] . " " . $s_efektif; ?></b></span>
                                        </div>
                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="biaya_admin">Biaya Administrasi</label>
                                    <input type="text" class="form-control form-control-sm nominal" name="biaya_admin" id="biaya_admin" placeholder="Biaya Administrasi" value="<?= $data_edit['biaya_admin'] ?>">
                                </div>
                                <div class="form-group">
                                    <label for="tnaggal">Tanggal Berlaku</label>
                                    <div class="input-group date" data-target-input="nearest">
                                        <input type="text" class="form-control form-control-sm datetimepicker-input tanggal" name="tanggal" autocomplete="off" data-toggle="datetimepicker" data-target="#datetimepicker" placeholder="yyyy-mm-dd">
                                        <div class="input-group-append" data-target="#datetimepicker">
                                            <div class="input-group-text"><i class="fa fa-calendar"></i></div>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div class="card-footer">
                                <button type="submit" name="simpanedit2" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card card-warning">
                            <div class="card-header">
                                <h3 class="card-title">Detail Histori</h3>
                            </div>
                            <div class="card-body">
                                <table class="table table-bordered table-striped">
                                    <thead>
                                        <tr>
                                            <th>#</th>
                                            <th>Jasa</th>
                                            <th>Administrasi</th>
                                            <th>Tanggal</th>
                                        </tr>
                                    </thead>
                                    <tbody>
                                        <?php
                                        $no = 1;
                                        $q_table = mysql_query("SELECT * FROM tb_produksimpanan_histori where sha1(id_produk) = '$_GET[id]'");
                                        while ($r_table = mysql_fetch_array($q_table)) {
                                        ?>
                                            <tr>
                                            <tr>
                                                <td><?= $no++; ?></td>
                                                <td class="text-right"><?= ($r_table['jasa'] * 100) . "%"; ?></td>
                                                <td class="text-right"><?= "Rp. " . number_format($r_table['biaya_admin'], 0, ',', '.'); ?></td>
                                                <td class="text-center"><?= $r_table['tanggal_berlaku']; ?></td>
                                            </tr>
                                            </tr>
                                        <?php
                                        }
                                        ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    <?php
        break;

    case 'edt1':
        $rek = "SELECT * FROM v_rekening";
        $data_edit = mysql_fetch_array(mysql_query("SELECT * from tb_produksimpanan where sha1(id_produk) = '$_GET[id]'"));
    ?>
        <div class="container-fluid">
            <form action="" method="POST">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Detail Produk</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="kode_produk">Kode Produk</label>
                                    <input type="text" class="form-control form-control-sm" name="kode_produk" value="<?= $data_edit['kode_produk']; ?>" id="kode_produk" readonly placeholder="Kode Produk">
                                </div>
                                <div class="form-group">
                                    <label for="nama_produk">Nama Produk</label>
                                    <input type="text" class="form-control form-control-sm" name="nama_produk" value="<?= $data_edit['nama_produk']; ?>" id="nama_produk" readonly placeholder="Nama Produk">
                                </div>
                            </div>

                            <div class="card-footer">
                                <button type="submit" name="simpanedit1" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card card-warning">
                            <div class="card-header">
                                <h3 class="card-title">Detail Rekening</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="rekening_debet_setoran">Rekening Debet Setoran</label>
                                    <select name="rekening_debet_setoran" id="rekening_debet_setoran" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            if ($r_rek['idrek4'] == $data_edit['idrek_debet_setoran']) {
                                                echo "<option selected value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                            } else {
                                                echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_kredit_setoran">Rekening Kredit Setoran</label>
                                    <select name="rekening_kredit_setoran" id="rekening_kredit_setoran" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            if ($r_rek['idrek4'] == $data_edit['idrek_kredit_setoran']) {
                                                echo "<option selected value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                            } else {
                                                echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_debet_penarikan">Rekening Debet Penarikan</label>
                                    <select name="rekening_debet_penarikan" id="rekening_debet_penarikan" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            if ($r_rek['idrek4'] == $data_edit['idrek_debet_penarikan']) {
                                                echo "<option selected value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                            } else {
                                                echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_kredit_penarikan">Rekening Kredit Penarikan</label>
                                    <select name="rekening_kredit_penarikan" id="rekening_kredit_penarikan" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            if ($r_rek['idrek4'] == $data_edit['idrek_kredit_penarikan']) {
                                                echo "<option selected value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                            } else {
                                                echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_debet_jasa">Rekening Debet Jasa</label>
                                    <select name="rekening_debet_jasa" id="rekening_debet_jasa" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            if ($r_rek['idrek4'] == $data_edit['idrek_debet_jasa']) {
                                                echo "<option selected value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                            } else {
                                                echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_kredit_jasa">Rekening Kredit Jasa</label>
                                    <select name="rekening_kredit_jasa" id="rekening_kredit_jasa" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            if ($r_rek['idrek4'] == $data_edit['idrek_kredit_jasa']) {
                                                echo "<option selected value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                            } else {
                                                echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_debet_admin">Rekening Debet Administrasi</label>
                                    <select name="rekening_debet_admin" id="rekening_debet_admin" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            if ($r_rek['idrek4'] == $data_edit['idrek_debet_administrasi']) {
                                                echo "<option selected value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                            } else {
                                                echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_kredit_admin">Rekening Kredit Administrasi</label>
                                    <select name="rekening_kredit_admin" id="rekening_kredit_admin" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            if ($r_rek['idrek4'] == $data_edit['idrek_kredit_administrasi']) {
                                                echo "<option selected value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                            } else {
                                                echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    <?php
        break;
    case 'ins':
        $rek = "SELECT * FROM v_rekening";
    ?>
        <div class="container-fluid">
            <form action="" method="POST">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Detail Produk</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="kode_produk">Kode Produk</label>
                                    <input type="text" class="form-control form-control-sm" name="kode_produk" id="kode_produk" placeholder="Kode Produk">
                                </div>
                                <div class="form-group">
                                    <label for="nama_produk">Nama Produk</label>
                                    <input type="text" class="form-control form-control-sm" name="nama_produk" id="nama_produk" placeholder="Nama Produk">
                                </div>
                                <div class="form-group">
                                    <label for="nama_produk">Jasa Produk</label>
                                    <div class="input-group">
                                        <input type="text" name="jasa" class="form-control">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><b>%</b></span>
                                        </div>
                                        <div class="input-group-append">
                                            <select type="text" name="hitung" id="hitung" class="form-control select2">
                                                <option value="">--Pilih Hitung--</option>
                                                <?php
                                                $q_hit = mysql_query("SELECT * FROM tb_metodehitungjasa");
                                                while ($r_hit = mysql_fetch_array($q_hit)) {
                                                    echo "<option value='$r_hit[id]'>$r_hit[nama]</option>";
                                                }
                                                ?>
                                            </select>
                                        </div>

                                        <div class="input-group-append">
                                            <span class="input-group-text">
                                                <div class="form-check">
                                                    <input type="hidden" name="efektif" value="N">
                                                    <input class="form-check-input" name="efektif" value="Y" type="checkbox">
                                                    <label class="form-check-label">Efektif</label>
                                                </div>
                                            </span>
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="jangka_waktu">Jangka Waktu</label>
                                    <div class="input-group">
                                        <input type="number" name="jangka_waktu" class="form-control">
                                        <div class="input-group-append">
                                            <span class="input-group-text"><b>Bulan</b></span>
                                        </div>

                                    </div>
                                </div>
                                <div class="form-group">
                                    <label for="biaya_admin">Biaya Administrasi</label>
                                    <input type="text" class="form-control form-control-sm nominal" name="biaya_admin" id="biaya_admin" placeholder="Biaya Administrasi">
                                </div>
                            </div>

                            <div class="card-footer">
                                <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card card-warning">
                            <div class="card-header">
                                <h3 class="card-title">Detail Rekening</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="rekening_debet_setoran">Rekening Debet Setoran</label>
                                    <select name="rekening_debet_setoran" id="rekening_debet_setoran" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_kredit_setoran">Rekening Kredit Setoran</label>
                                    <select name="rekening_kredit_setoran" id="rekening_kredit_setoran" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_debet_penarikan">Rekening Debet Penarikan</label>
                                    <select name="rekening_debet_penarikan" id="rekening_debet_penarikan" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_kredit_penarikan">Rekening Kredit Penarikan</label>
                                    <select name="rekening_kredit_penarikan" id="rekening_kredit_penarikan" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_debet_jasa">Rekening Debet Jasa</label>
                                    <select name="rekening_debet_jasa" id="rekening_debet_jasa" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_kredit_jasa">Rekening Kredit Jasa</label>
                                    <select name="rekening_kredit_jasa" id="rekening_kredit_jasa" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_debet_admin">Rekening Debet Administrasi</label>
                                    <select name="rekening_debet_admin" id="rekening_debet_admin" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_kredit_admin">Rekening Kredit Administrasi</label>
                                    <select name="rekening_kredit_admin" id="rekening_kredit_admin" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
    <?php
        break;

    default:
    ?>
        <div class="container-fluid">
            <div class="col-12">
                <div class="card">
                    <div class="card-header ui-sortable-handle">
                        <h3 class="card-title">Daftar Produk Simpanan</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Minimize data">
                                <i class="fas fa-minus"></i>
                            </button>
                            <a href="?page=simpanan&act=ins" class="btn btn-tool" title="Tambah Simpanan Modal">
                                <i class="fas fa-plus"></i>
                            </a>
                            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Tutup Data">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center" width="5%">No.</th>
                                    <th class="text-center">Kode</th>
                                    <th class="text-center">Nama Produk</th>
                                    <th class="text-center">Jasa</th>
                                    <th class="text-center"><i class="fas fa-cog"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                $q_simo = mysql_query("SELECT * FROM tb_produksimpanan");
                                while ($r_simo = mysql_fetch_array($q_simo)) {
                                ?>
                                    <tr>
                                        <td><?= $no++; ?></td>
                                        <td><?= $r_simo['kode_produk']; ?></td>
                                        <td><?= $r_simo['nama_produk']; ?></td>
                                        <td class="text-right"><?= ($r_simo['jasa'] * 100) . "%"; ?></td>
                                        <td class="text-center">
                                            <a href="?page=simpanan&act=edt1&id=<?= sha1($r_simo['id_produk']) ?>" class="btn btn-info btn-sm" title="Edit Rekening"><i class="fas fa-edit"></i></a>
                                            <a href="?page=simpanan&act=edt2&id=<?= sha1($r_simo['id_produk']) ?>" class="btn btn-warning btn-sm" title="Edit Biaya Jasa"><i class="fas fa-edit"></i></a>
                                            <a href="?page=simpanan&act=del&id=<?= sha1($r_simo['id_produk']) ?>" class="btn btn-danger btn-sm" title="Edit Biaya Jasa"><i class="fas fa-trash"></i></a>
                                        </td>
                                    </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        </script>
<?php
        break;
}
