<?php
if (isset($_POST["simpanedit"])) {
    $setoran_tetap = hilangTitik($_POST['setoran_tetap']);
    $max_simpanan = hilangTitik($_POST['max_simpanan']);
    $edt = mysql_query("UPDATE tb_produksimpananmodal SET 
    nama_produk = '$_POST[nama_produk]',
    setoran_tetap = '$setoran_tetap',
    limit_simpanan = '$max_simpanan',
    idrek_debet_simpanan = '$_POST[rekening_debet_setoran]',
    idrek_kredit_simpanan = '$_POST[rekening_kredit_setoran]',
    idrek_debet_penarikan = '$_POST[rekening_debet_penarikan]',
    idrek_kredit_penarikan = '$_POST[rekening_kredit_penarikan]'
    WHERE sha1(id_produk) = '$_GET[id]'");
    if ($edt) {
        echo "<script>
        document.location=\"?page=simpananmodal\"
        </script>";
    } else {
        echo "<script>
        alert(\"Gagal\")
        document.location=\"?page=simpananmodal\"
        </script>";
    }
}

if (isset($_POST['simpan'])) {
    $setoran_tetap = hilangTitik($_POST['setoran_tetap']);
    $max_simpanan = hilangTitik($_POST['max_simpanan']);
    $ins = mysql_query("INSERT INTO tb_produksimpananmodal (
    kode_produk,
    nama_produk,
    setoran_tetap,
    limit_simpanan,
    idrek_debet_simpanan,
    idrek_kredit_simpanan,
    idrek_debet_penarikan, 
    idrek_kredit_penarikan) VALUES (
    '$_POST[kode_produk]',
    '$_POST[nama_produk]',
    '$setoran_tetap',
    '$max_simpanan',
    '$_POST[rekening_debet_setoran]',
    '$_POST[rekening_kredit_setoran]',
    '$_POST[rekening_debet_penarikan]',
    '$_POST[rekening_kredit_penarikan]'
    )");

    if ($ins) {
        echo "<script>
        document.location=\"?page=simpananmodal\"
        </script>";
    } else {
        echo "<script>
        alert(\"Gagal\")
        document.location=\"?page=simpananmodal\"
        </script>";
    }
}

switch ($_GET['act']) {
    case 'edt':
        $data_edit = mysql_fetch_array(mysql_query("SELECT * from tb_produksimpananmodal where sha1(id_produk) = '$_GET[id]'"));
        $rek = "SELECT * FROM v_rekening";
?>
        <div class="container-fluid">
            <form action="" method="POST">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Detail Produk</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="kode_produk">Kode Produk</label>
                                    <input type="text" class="form-control form-control-sm" name="kode_produk" readonly id="kode_produk" value="<?= $data_edit['kode_produk'] ?>" placeholder="Kode Produk">
                                </div>
                                <div class="form-group">
                                    <label for="nama_produk">Nama Produk</label>
                                    <input type="text" class="form-control form-control-sm" name="nama_produk" id="nama_produk" value="<?= $data_edit['nama_produk'] ?>" placeholder="Nama Produk">
                                </div>
                                <div class="form-group">
                                    <label for="setoran_tetap">Setoran Tetap</label>
                                    <input type="text" class="form-control form-control-sm nominal" name="setoran_tetap" id="setoran_tetap" value="<?= $data_edit['setoran_tetap'] ?>" placeholder="Setoran Tetap">
                                </div>
                                <div class="form-group">
                                    <label for="max_simpanan">Max Simpanan</label>
                                    <input type="text" class="form-control form-control-sm nominal" name="max_simpanan" id="max_simpanan" value="<?= $data_edit['limit_simpanan'] ?>" placeholder="Max Simpanan">
                                </div>
                            </div>

                            <div class="card-footer">
                                <button type="submit" name="simpanedit" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card card-warning">
                            <div class="card-header">
                                <h3 class="card-title">Detail Rekening</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="rekening_debet_setoran">Rekening Debet Setoran</label>
                                    <select name="rekening_debet_setoran" id="rekening_debet_setoran" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            if ($r_rek['idrek4'] == $data_edit['idrek_debet_simpanan']) {
                                                echo "<option selected value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                            } else {
                                                echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_kredit_setoran">Rekening Kredit Setoran</label>
                                    <select name="rekening_kredit_setoran" id="rekening_kredit_setoran" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            if ($r_rek['idrek4'] == $data_edit['idrek_kredit_simpanan']) {
                                                echo "<option selected value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                            } else {
                                                echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_debet_penarikan">Rekening Debet Penarikan</label>
                                    <select name="rekening_debet_penarikan" id="rekening_debet_penarikan" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            if ($r_rek['idrek4'] == $data_edit['idrek_debet_penarikan']) {
                                                echo "<option selected value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                            } else {
                                                echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_kredit_penarikan">Rekening Kredit Penarikan</label>
                                    <select name="rekening_kredit_penarikan" id="rekening_kredit_penarikan" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            if ($r_rek['idrek4'] == $data_edit['idrek_kredit_penarikan']) {
                                                echo "<option selected value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                            } else {
                                                echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                            }
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <script>
            $('#rekening_debet_setoran, #rekening_kredit_setoran, #rekening_debet_penarikan, #rekening_kredit_penarikan').select2();
        </script>
    <?php
        break;
    case 'ins':
        $rek = "SELECT * FROM v_rekening";
    ?>
        <div class="container-fluid">
            <form action="" method="POST">
                <div class="row">
                    <div class="col-md-6">
                        <div class="card card-primary">
                            <div class="card-header">
                                <h3 class="card-title">Detail Produk</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="kode_produk">Kode Produk</label>
                                    <input type="text" class="form-control form-control-sm" name="kode_produk" id="kode_produk" placeholder="Kode Produk">
                                </div>
                                <div class="form-group">
                                    <label for="nama_produk">Nama Produk</label>
                                    <input type="text" class="form-control form-control-sm" name="nama_produk" id="nama_produk" placeholder="Nama Produk">
                                </div>
                                <div class="form-group">
                                    <label for="setoran_tetap">Setoran Tetap</label>
                                    <input type="text" class="form-control form-control-sm nominal" name="setoran_tetap" id="setoran_tetap" placeholder="Setoran Tetap">
                                </div>
                                <div class="form-group">
                                    <label for="max_simpanan">Max Simpanan</label>
                                    <input type="text" class="form-control form-control-sm nominal" name="max_simpanan" id="max_simpanan" placeholder="Max Simpanan">
                                </div>
                            </div>

                            <div class="card-footer">
                                <button type="submit" name="simpan" class="btn btn-primary">Simpan</button>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="card card-warning">
                            <div class="card-header">
                                <h3 class="card-title">Detail Rekening</h3>
                            </div>
                            <div class="card-body">
                                <div class="form-group">
                                    <label for="rekening_debet_setoran">Rekening Debet Setoran</label>
                                    <select name="rekening_debet_setoran" id="rekening_debet_setoran" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_kredit_setoran">Rekening Kredit Setoran</label>
                                    <select name="rekening_kredit_setoran" id="rekening_kredit_setoran" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_debet_penarikan">Rekening Debet Penarikan</label>
                                    <select name="rekening_debet_penarikan" id="rekening_debet_penarikan" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                                <div class="form-group">
                                    <label for="rekening_kredit_penarikan">Rekening Kredit Penarikan</label>
                                    <select name="rekening_kredit_penarikan" id="rekening_kredit_penarikan" class="form-control form-control-sm select2">
                                        <option value="">--Pilih Rekening--</option>
                                        <?php
                                        $q_rek = mysql_query($rek);
                                        while ($r_rek = mysql_fetch_array($q_rek)) {
                                            echo "<option value='$r_rek[idrek4]'>$r_rek[kd_rek] - $r_rek[namarek4]</option>";
                                        }
                                        ?>
                                    </select>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </form>
        </div>
        <script>
            // $('#rekening_debet_setoran, #rekening_kredit_setoran, #rekening_debet_penarikan, #rekening_kredit_penarikan').select2();
        </script>
    <?php
        break;

    default:
    ?>
        <div class="container-fluid">
            <div class="col-12">
                <div class="card">
                    <div class="card-header ui-sortable-handle">
                        <h3 class="card-title">Daftar Produk Simpanan Modal</h3>

                        <div class="card-tools">
                            <button type="button" class="btn btn-tool" data-card-widget="collapse" title="Minimize data">
                                <i class="fas fa-minus"></i>
                            </button>
                            <a href="?page=simpananmodal&act=ins" class="btn btn-tool" title="Tambah Simpanan Modal">
                                <i class="fas fa-plus"></i>
                            </a>
                            <button type="button" class="btn btn-tool" data-card-widget="remove" title="Tutup Data">
                                <i class="fas fa-times"></i>
                            </button>
                        </div>
                    </div>
                    <div class="card-body">
                        <table id="example2" class="table table-bordered table-hover">
                            <thead>
                                <tr>
                                    <th class="text-center" width="5%">No.</th>
                                    <th class="text-center">Kode</th>
                                    <th class="text-center">Nama Produk</th>
                                    <th class="text-center">Setoran Tetap</th>
                                    <th class="text-center"><i class="fas fa-cog"></i></th>
                                </tr>
                            </thead>
                            <tbody>
                                <?php
                                $no = 1;
                                $q_simo = mysql_query("SELECT * FROM tb_produksimpananmodal");
                                while ($r_simo = mysql_fetch_array($q_simo)) {
                                ?>
                                    <tr>
                                        <td><?= $no++; ?></td>
                                        <td><?= $r_simo['kode_produk']; ?></td>
                                        <td><?= $r_simo['nama_produk']; ?></td>
                                        <td class="text-right"><?= "Rp." . number_format($r_simo['setoran_tetap'], 0, ',', '.') ?></td>
                                        <td class="text-center"><a href="?page=simpananmodal&act=edt&id=<?= sha1($r_simo['id_produk']) ?>" class="btn btn-warning btn-sm"><i class="fas fa-edit"></i></a></td>
                                    </tr>
                                <?php
                                }
                                ?>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>

        <script>
            $('#example2').DataTable({
                "paging": true,
                "lengthChange": false,
                "searching": true,
                "ordering": false,
                "info": true,
                "autoWidth": false,
                "responsive": true,
            });
        </script>
<?php
        break;
}
?>