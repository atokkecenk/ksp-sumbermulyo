<?php
error_reporting(0);
include 'db.php';

function add_nol($jumlah)
{
    $agk = '';
    switch ($jumlah) {
        case '1':
            $agk = '0';
            break;
        case '2':
            $agk = '00';
            break;
        case '3':
            $agk = '000';
            break;
        case '4':
            $agk = '0000';
            break;
        case '5':
            $agk = '00000';
            break;
        case '6':
            $agk = '000000';
            break;
        case '7':
            $agk = '0000000';
            break;
        case '8':
            $agk = '00000000';
            break;
        case '9':
            $agk = '000000000';
            break;
        case '10':
            $agk = '0000000000';
            break;
    }
    return $agk;
}

function buat_kode($tabel, $field, $like, $jumlah)
{

    $nomor = mysql_fetch_array(mysql_query("SELECT max(RIGHT($field,$jumlah)) AS no FROM $tabel WHERE $field like '$like%'"));
    $ns = $nomor['no'];
    $last_digit = $ns;
    // $new_digit = '000000' . ($last_digit + 1);
    // $new_nomor = $like . substr($new_digit, -6);
    $new_digit = add_nol($jumlah) . ($last_digit + 1);
    $new_nomor = $like . substr($new_digit, -$jumlah);
    return $new_nomor;
}



function get_kelamin($params = '')
{
    $where = ($params !== '') ? 'WHERE ' . $params : '';
    $sql = mysql_query("SELECT * FROM tb_kelamin $where") or die(mysql_error());
    $data = [];
    while ($rows = mysql_fetch_array($sql)) {
        $data[] = $rows;
    }
    return $data;
}

function get_status_anggota($params = '')
{
    $where = ($params !== '') ? 'WHERE ' . $params : '';
    $sql = mysql_query("SELECT * FROM tb_sts_anggota $where") or die(mysql_error());
    $data = [];
    while ($rows = mysql_fetch_array($sql)) {
        $data[] = $rows;
    }
    return $data;
}

function cek_ktp($ktp)
{
    $sql = mysql_query("SELECT * FROM tb_anggota WHERE ktp = '$ktp'") or die(mysql_error());
    if (mysql_num_rows($sql) == 0) {
        return 0;
    } else {
        return 1;
    }
}

function get_anggota($type, $return = '', $params = '')
{
    $where = ($params !== '') ? 'WHERE ' . $params : '';
    $sql = mysql_query("SELECT
                            c.`status` as sts_anggota,
                            b.icon,
                            b.kelamin as jk,
                            a.* 
                        FROM
                            `tb_anggota` a
                            LEFT JOIN tb_kelamin b ON a.kelamin = b.id
                            LEFT JOIN tb_sts_anggota c ON a.status_anggota = c.id
                            $where
                        ORDER BY
	                        a.id ASC
                            ") or die(mysql_error());

    $data = [];
    if ($type == 'row' && $return !== '' && $params !== '') {
        $data = mysql_fetch_array($sql);
        return $data[$return];
    } elseif ($type == 'result') {
        while ($rows = mysql_fetch_array($sql)) {
            $data[] = $rows;
        }
        return $data;
    }
}

function get_agama($type, $return = '', $params = '')
{
    $where = ($params !== '') ? 'WHERE ' . $params : '';
    $sql = mysql_query("SELECT * FROM tb_agama $where ") or die(mysql_error());

    $data = [];
    if ($type == 'row' && $return !== '' && $params !== '') {
        $data = mysql_fetch_array($sql);
        return $data[$return];
    } elseif ($type == 'result') {
        while ($rows = mysql_fetch_array($sql)) {
            $data[] = $rows;
        }
        return $data;
    }
}

function get_karyawan($type, $return = '', $params = '')
{
    $where = ($params !== '') ? 'WHERE ' . $params : '';
    $sql = mysql_query("SELECT
                        a.id_karyawan,
                        a.kd_karyawan,
                        a.nama,
                        a.alamat,
                        b.id as id_kelamin,
                        b.kelamin,
                        c.id as id_agama,
                        c.agama,
                        a.email,
                        a.username,
                        a.jenis_identitas,
                        a.`level` AS id_lev,
                        d.`level`,
                        a.menu_akses,
                        a.foto,
                        a.register,
                        a.last_login,
                        a.na
                    FROM
                        `tb_karyawan` a
                        LEFT JOIN tb_kelamin b ON a.sex = b.id
                        LEFT JOIN tb_agama c ON a.agama = c.id
                        LEFT JOIN tb_user d ON a.`level` = d.id
                    $where
                    ORDER BY
	                    a.id_karyawan ASC
                    ") or die(mysql_error());

    $data = [];
    if ($type == 'row' && $return !== '' && $params !== '') {
        $data = mysql_fetch_array($sql);
        return $data[$return];
    } elseif ($type == 'result') {
        while ($rows = mysql_fetch_array($sql)) {
            $data[] = $rows;
        }
        return $data;
    }
}

function get_user_level($type, $return = '', $params = '')
{
    $where = ($params !== '') ? 'WHERE ' . $params : '';
    $sql = mysql_query("SELECT * FROM tb_user $where ") or die(mysql_error());

    $data = [];
    if ($type == 'row' && $return !== '' && $params !== '') {
        $data = mysql_fetch_array($sql);
        return $data[$return];
    } elseif ($type == 'result') {
        while ($rows = mysql_fetch_array($sql)) {
            $data[] = $rows;
        }
        return $data;
    }
}

function get_performa($type, $return = '', $params = '')
{
    $where = ($params !== '') ? 'WHERE ' . $params : '';
    $sql = mysql_query("SELECT * FROM tb_performa $where ") or die(mysql_error());

    $data = [];
    if ($type == 'row' && $return !== '' && $params !== '') {
        $data = mysql_fetch_array($sql);
        return $data[$return];
    } elseif ($type == 'result') {
        while ($rows = mysql_fetch_array($sql)) {
            $data[] = $rows;
        }
        return $data;
    }
}

function get_jurnal_bykd_anggota($type, $return = '', $params = '')
{
    $where = ($params !== '') ? 'WHERE ' . $params : '';
    $sql = mysql_query("SELECT 
                            a.*,
                            CONCAT( a.kdrek1, ' ', a.kdrek2, ' ', a.kdrek3, ' ', a.kdrek4 ) AS kd_rek 
                        FROM `tb_jurnal` a 
                            $where
                        ") or die(mysql_error());
    $data = [];
    if ($type == 'row' && $return !== '' && $params !== '') {
        $data = mysql_fetch_array($sql);
        return $data[$return];
    } elseif ($type == 'result') {
        while ($rows = mysql_fetch_array($sql)) {
            $data[] = $rows;
        }
        return $data;
    }
}

function __setting($return)
{
    $data = [];
    $sql = mysql_query("SELECT * FROM tb_setting WHERE id = 1") or die(mysql_error());
    $data = mysql_fetch_array($sql);
    return $data[$return];
}

// From method post
if (isset($_POST['post'])) {
    $mode = $_POST['mode'];
    switch ($mode) {
        case 'cekUsername':
            $user = $_POST['user'];
            $sql = mysql_query("SELECT * FROM tb_karyawan WHERE username = '$user'") or die(mysql_error());
            if (mysql_num_rows($sql) == 0) {
                echo 0;
            } else {
                echo 1;
            }
            break;
    }
}
